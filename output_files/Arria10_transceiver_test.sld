<sld_project_info>
  <sld_infos>
    <sld_info hpath="TTC_PHY_LAYER:TTC_PHY_LAYER_comp|ATX_PLL_RESET:ATX_PLL_RESET_comp" name="ATX_PLL_RESET_comp">
      <assignment_values>
        <assignment_value text="QSYS_NAME ATX_PLL_RESET HAS_SOPCINFO 1 GENERATION_ID 1462231382"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="TTC_PHY_LAYER:TTC_PHY_LAYER_comp|Rx_PHY_RESET:Rx_PHY_RESET_comp" name="Rx_PHY_RESET_comp">
      <assignment_values>
        <assignment_value text="QSYS_NAME Rx_PHY_RESET HAS_SOPCINFO 1 GENERATION_ID 1462231374"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="TTC_PHY_LAYER:TTC_PHY_LAYER_comp|TX_ATX_PLL:TX_ATX_PLL_comp" name="TX_ATX_PLL_comp">
      <assignment_values>
        <assignment_value text="QSYS_NAME TX_ATX_PLL HAS_SOPCINFO 1 GENERATION_ID 1462231356"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="TTC_PHY_LAYER:TTC_PHY_LAYER_comp|TX_RX_PHY:TX_RX_PHY_comp" name="TX_RX_PHY_comp">
      <assignment_values>
        <assignment_value text="QSYS_NAME TX_RX_PHY HAS_SOPCINFO 1 GENERATION_ID 1462231394"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="TTC_PHY_LAYER:TTC_PHY_LAYER_comp|Tx_PHY_RESET:Tx_PHY_RESET_comp" name="Tx_PHY_RESET_comp">
      <assignment_values>
        <assignment_value text="QSYS_NAME Tx_PHY_RESET HAS_SOPCINFO 1 GENERATION_ID 1462231366"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="phase_calc:phase_calc_comp|delay_insert_pll:u0" name="u0">
      <assignment_values>
        <assignment_value text="QSYS_NAME delay_insert_pll HAS_SOPCINFO 1 GENERATION_ID 1462231459"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="phase_sample_clk_pll:phase_sample_clk_pll_comp" name="phase_sample_clk_pll_comp">
      <assignment_values>
        <assignment_value text="QSYS_NAME phase_sample_clk_pll HAS_SOPCINFO 1 GENERATION_ID 1462283235"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="sld_hub:auto_hub|alt_sld_fab:\instrumentation_fabric_with_node_gen:instrumentation_fabric" library="alt_sld_fab" name="instrumentation_fabric">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_sld_fab HAS_SOPCINFO 1"/>
      </assignment_values>
    </sld_info>
  </sld_infos>
</sld_project_info>
