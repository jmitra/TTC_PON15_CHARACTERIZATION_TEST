echo "Cleaning Project Directory"
rm -r db
rm -r incremental_db
rm -r output_files
rm *.sopcinfo
rm *.json
rm -r ./Arria10/ATX_PLL_RESET
rm -r ./Arria10/ISSP
rm -r ./Arria10/Rx_PHY_RESET
rm -r ./Arria10/TX_ATX_PLL
rm -r ./Arria10/Tx_PHY_RESET
rm -r ./Arria10/TX_RX_PHY
rm -r ./Arria10/FPLL_CASCADE_SOURCE
rm -r ./Arria10/IOPLL
rm -r ./Arria10/temp_measure_ip
rm -r ./Arria10/phase_sample_clk_fpll
rm -r ./Arria10/delay_insert_pll
rm *.sopcinfo
rm PLLJ_PLLSPE_INFO.txt