--------------==============================================------------------------

This design is used for characterization of TTC-PON receiving side.
Arria 10 Development board is used for this design.
The communication design is tested between Arria10 and Kintex 7 / Ultrascale.

--------------==============================================------------------------

To execute the code from terminal use the script files within Project Folder.

clean.sh -- Clears all the directories for fresh start
gen_ip.sh -- If IP generation is needed for both synthesis and simulation (optional)
compile.sh -- For Compilation
prog.sh -- To program the Arria 10 board

-------------===============================================-------------------------

To monitor and control the signals use Signal tap (.stp) file  and In-System Sources and Probes files (.spf)
