## Generated SDC file "Arria10_transceiver_test.out.sdc"

## Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, the Altera Quartus Prime License Agreement,
## the Altera MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Altera and sold by Altera or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus Prime"
## VERSION "Version 15.1.2 Build 193 02/01/2016 SJ Standard Edition"

## DATE    "Wed May  4 15:18:23 2016"

##
## DEVICE  "10AX115S3F45E2SG"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {~ALTERA_CLKUSR~} -period 8.000 -waveform { 0.000 4.000 } [get_pins -compatibility_mode {~ALTERA_CLKUSR~~ibuf|o}]
create_clock -name {altera_reserved_tck} -period 33.333 -waveform { 0.000 16.666 } [get_ports {altera_reserved_tck}]
create_clock -name {refclk_sfp_p} -period 4.166 -waveform { 0.000 2.083 } [get_ports {refclk_sfp_p}]
create_clock -name {clk_50} -period 20.000 -waveform { 0.000 10.000 } [get_ports {clk_50}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout} -source [get_pins {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|g_xcvr_native_insts[0].twentynm_xcvr_native_inst|twentynm_xcvr_native_inst|inst_twentynm_pcs|gen_twentynm_hssi_rx_pcs_pma_interface.inst_twentynm_hssi_rx_pcs_pma_interface|pma_rx_pma_clk}] -duty_cycle 50.000 -multiply_by 1 -master_clock {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk} [get_pins {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|g_xcvr_native_insts[0].twentynm_xcvr_native_inst|twentynm_xcvr_native_inst|inst_twentynm_pcs|gen_twentynm_hssi_rx_pld_pcs_interface.inst_twentynm_hssi_rx_pld_pcs_interface|pld_pcs_rx_clk_out}] 
create_generated_clock -name {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout} -source [get_pins {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|g_xcvr_native_insts[0].twentynm_xcvr_native_inst|twentynm_xcvr_native_inst|inst_twentynm_pcs|gen_twentynm_hssi_tx_pcs_pma_interface.inst_twentynm_hssi_tx_pcs_pma_interface|pma_tx_pma_clk}] -duty_cycle 50.000 -multiply_by 1 -master_clock {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]} [get_pins {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|g_xcvr_native_insts[0].twentynm_xcvr_native_inst|twentynm_xcvr_native_inst|inst_twentynm_pcs|gen_twentynm_hssi_tx_pld_pcs_interface.inst_twentynm_hssi_tx_pld_pcs_interface|pld_pcs_tx_clk_out}] 
create_generated_clock -name {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk} -source [get_pins {IOPLL_comp|iopll_0|altera_pll_i|twentynm_pll|outclk[0]~CLKENA0|outclk}] -duty_cycle 50.000 -multiply_by 1 -master_clock {IOPLL_comp|iopll_0|outclk0} [get_pins {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|g_xcvr_native_insts[0].twentynm_xcvr_native_inst|twentynm_xcvr_native_inst|inst_twentynm_xcvr_avmm|avmm_atom_insts[0].twentynm_hssi_avmm_if_inst|avmmclk}] 
create_generated_clock -name {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk} -source [get_pins {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|g_xcvr_native_insts[0].twentynm_xcvr_native_inst|twentynm_xcvr_native_inst|inst_twentynm_pma|gen_twentynm_hssi_pma_cdr_refclk_select_mux.inst_twentynm_hssi_pma_cdr_refclk_select_mux|ref_iqclk[11]}] -duty_cycle 50.000 -multiply_by 1 -master_clock {refclk_sfp_p} [get_pins {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|g_xcvr_native_insts[0].twentynm_xcvr_native_inst|twentynm_xcvr_native_inst|inst_twentynm_pma|gen_twentynm_hssi_pma_rx_deser.inst_twentynm_hssi_pma_rx_deser|clkdiv}] 
create_generated_clock -name {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]} -source [get_pins {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|a10_xcvr_atx_pll_inst|twentynm_hssi_pma_lc_refclk_select_mux_inst|ref_iqclk[11]}] -duty_cycle 50.000 -multiply_by 1 -master_clock {refclk_sfp_p} -invert [get_pins {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|a10_xcvr_atx_pll_inst|twentynm_hssi_pma_cgb_master_inst|cpulse_out_bus[0]}] 
create_generated_clock -name {IOPLL_comp|iopll_0|outclk0} -source [get_pins {IOPLL_comp|iopll_0|altera_pll_i|twentynm_pll|iopll_inst|refclk[0]}] -duty_cycle 50.000 -multiply_by 15 -divide_by 6 -master_clock {clk_50} [get_pins {IOPLL_comp|iopll_0|altera_pll_i|twentynm_pll|iopll_inst|outclk[0]}] 
create_generated_clock -name {phase_sample_clk_pll_comp|iopll_0|outclk0} -source [get_pins {phase_sample_clk_pll_comp|iopll_0|altera_pll_i|twentynm_pll|iopll_inst|refclk[0]}] -duty_cycle 50.000 -multiply_by 67 -divide_by 147 -master_clock {refclk_sfp_p} [get_pins {phase_sample_clk_pll_comp|iopll_0|altera_pll_i|twentynm_pll|iopll_inst|outclk[0]}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {altera_reserved_tck}] -rise_to [get_clocks {altera_reserved_tck}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {altera_reserved_tck}] -fall_to [get_clocks {altera_reserved_tck}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {altera_reserved_tck}] -rise_to [get_clocks {altera_reserved_tck}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {altera_reserved_tck}] -fall_to [get_clocks {altera_reserved_tck}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {~ALTERA_CLKUSR~}] -rise_to [get_clocks {~ALTERA_CLKUSR~}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {~ALTERA_CLKUSR~}] -fall_to [get_clocks {~ALTERA_CLKUSR~}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {~ALTERA_CLKUSR~}] -rise_to [get_clocks {~ALTERA_CLKUSR~}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {~ALTERA_CLKUSR~}] -fall_to [get_clocks {~ALTERA_CLKUSR~}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -rise_to [get_clocks {refclk_sfp_p}]  0.230  
set_clock_uncertainty -rise_from [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -fall_to [get_clocks {refclk_sfp_p}]  0.230  
set_clock_uncertainty -fall_from [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -rise_to [get_clocks {refclk_sfp_p}]  0.230  
set_clock_uncertainty -fall_from [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -fall_to [get_clocks {refclk_sfp_p}]  0.230  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.050  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.050  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.250  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.250  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}]  0.240  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}]  0.240  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}]  0.050  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}]  0.050  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.240  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.240  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.125  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.140  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.125  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.140  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {~ALTERA_CLKUSR~}]  0.210  
set_clock_uncertainty -rise_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {~ALTERA_CLKUSR~}]  0.210  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.050  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.050  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.250  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.250  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}]  0.240  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}]  0.240  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}]  0.050  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}]  0.050  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.240  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.240  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.125  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.140  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.125  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.140  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -rise_to [get_clocks {~ALTERA_CLKUSR~}]  0.210  
set_clock_uncertainty -fall_from [get_clocks {IOPLL_comp|iopll_0|outclk0}] -fall_to [get_clocks {~ALTERA_CLKUSR~}]  0.210  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.010  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.010  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.125  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.125  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.010  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.010  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.125  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.125  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.050  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.050  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}]  0.050  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}]  0.050  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.050  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.050  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}]  0.050  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|avmmclk}]  0.050  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {refclk_sfp_p}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {refclk_sfp_p}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {refclk_sfp_p}] -setup 0.140  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {refclk_sfp_p}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {refclk_sfp_p}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -rise_to [get_clocks {refclk_sfp_p}] -hold 0.246  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {refclk_sfp_p}] -setup 0.140  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}] -fall_to [get_clocks {refclk_sfp_p}] -hold 0.246  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.270  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.270  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.250  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.250  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.110  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.110  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {refclk_sfp_p}]  0.080  
set_clock_uncertainty -rise_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {refclk_sfp_p}]  0.080  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.270  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.270  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.250  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {IOPLL_comp|iopll_0|outclk0}]  0.250  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.110  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|tx_clkout}]  0.110  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -rise_to [get_clocks {refclk_sfp_p}]  0.080  
set_clock_uncertainty -fall_from [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_clkout}] -fall_to [get_clocks {refclk_sfp_p}]  0.080  
set_clock_uncertainty -rise_from [get_clocks {refclk_sfp_p}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.230  
set_clock_uncertainty -rise_from [get_clocks {refclk_sfp_p}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.230  
set_clock_uncertainty -rise_from [get_clocks {refclk_sfp_p}] -rise_to [get_clocks {refclk_sfp_p}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {refclk_sfp_p}] -fall_to [get_clocks {refclk_sfp_p}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {refclk_sfp_p}] -rise_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.230  
set_clock_uncertainty -fall_from [get_clocks {refclk_sfp_p}] -fall_to [get_clocks {phase_sample_clk_pll_comp|iopll_0|outclk0}]  0.230  
set_clock_uncertainty -fall_from [get_clocks {refclk_sfp_p}] -rise_to [get_clocks {refclk_sfp_p}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {refclk_sfp_p}] -fall_to [get_clocks {refclk_sfp_p}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {~ALTERA_CLKUSR~}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.070  
set_clock_uncertainty -rise_from [get_clocks {~ALTERA_CLKUSR~}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.070  
set_clock_uncertainty -rise_from [get_clocks {~ALTERA_CLKUSR~}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}]  0.070  
set_clock_uncertainty -rise_from [get_clocks {~ALTERA_CLKUSR~}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}]  0.070  
set_clock_uncertainty -fall_from [get_clocks {~ALTERA_CLKUSR~}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.070  
set_clock_uncertainty -fall_from [get_clocks {~ALTERA_CLKUSR~}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_RX_PHY_comp|xcvr_native_a10_0|rx_pma_clk}]  0.070  
set_clock_uncertainty -fall_from [get_clocks {~ALTERA_CLKUSR~}] -rise_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}]  0.070  
set_clock_uncertainty -fall_from [get_clocks {~ALTERA_CLKUSR~}] -fall_to [get_clocks {TTC_PHY_LAYER_comp|TX_ATX_PLL_comp|xcvr_atx_pll_a10_0|tx_bonding_clocks[0]}]  0.070  


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 


#**************************************************************
# Set False Path
#**************************************************************

set_false_path -to [get_registers {*alt_xcvr_resync*sync_r[0]}]
set_false_path -to [get_keepers {*altera_std_synchronizer:*|din_s1}]
set_false_path -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_10g_krfec_rx_pld_rst_n*}]
set_false_path -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_8g_g3_rx_pld_rst_n*}]
set_false_path -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_pmaif_rx_pld_rst_n*}]
set_false_path -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_bitslip*}]
set_false_path -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_rx_prbs_err_clr*}]
set_false_path -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_polinv_tx*}]
set_false_path -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_polinv_rx*}]
set_false_path -through [get_pins -compatibility_mode {*xcvr_native*optional_chnl_reconfig_logic*prbs_accumulators_enable*prbs_soft_accumulators|rx_clk_reset_sync*sync_r*clrn}] -to [get_registers {*xcvr_native*optional_chnl_reconfig_logic*prbs_accumulators_enable*prbs_soft_accumulators|rx_clk_reset_sync*sync_r[?]}]
set_false_path -from [get_registers {*xcvr_native*optional_chnl_reconfig_logic*avmm_csr_enabled*embedded_debug_soft_csr*prbs_reg*}] -to [get_registers {*xcvr_native*optional_chnl_reconfig_logic*prbs_accumulators_enable*prbs_soft_accumulators|rx_clk_prbs_reset_sync*sync_r[?]}]


#**************************************************************
# Set Multicycle Path
#**************************************************************

set_multicycle_path -hold -end -from [get_registers {*xcvr_native*optional_chnl_reconfig_logic*prbs_accumulators_enable*prbs_soft_accumulators|rx_prbs_err_snapshot*}] -to [get_registers {*xcvr_native*optional_chnl_reconfig_logic*prbs_accumulators_enable*prbs_soft_accumulators|avmm_prbs_err_count*}] 2
set_multicycle_path -setup -end -from [get_registers {*xcvr_native*optional_chnl_reconfig_logic*prbs_accumulators_enable*prbs_soft_accumulators|rx_prbs_err_snapshot*}] -to [get_registers {*xcvr_native*optional_chnl_reconfig_logic*prbs_accumulators_enable*prbs_soft_accumulators|avmm_prbs_err_count*}] 3


#**************************************************************
# Set Maximum Delay
#**************************************************************

set_max_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_pmaif_tx_pld_rst_n}] 50.000
set_max_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_8g_g3_tx_pld_rst_n}] 50.000
set_max_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_10g_krfec_tx_pld_rst_n}] 50.000
set_max_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_pma_txpma_rstb}] 20.000
set_max_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_pma_rxpma_rstb}] 20.000


#**************************************************************
# Set Minimum Delay
#**************************************************************

set_min_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_pmaif_tx_pld_rst_n}] -50.000
set_min_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_8g_g3_tx_pld_rst_n}] -50.000
set_min_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_10g_krfec_tx_pld_rst_n}] -50.000
set_min_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_pma_txpma_rstb}] -10.000
set_min_delay -to [get_pins -compatibility_mode {*twentynm_xcvr_native_inst|*inst_twentynm_pcs|*twentynm_hssi_*_pld_pcs_interface*|pld_pma_rxpma_rstb}] -10.000


#**************************************************************
# Set Input Transition
#**************************************************************

