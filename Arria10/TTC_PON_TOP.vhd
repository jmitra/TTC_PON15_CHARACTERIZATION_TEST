--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
-------------------------------------------------------------------------------
-- Title      : TTC_PON TOP LAYER VHDL FILE 
-- Project    : TTC-PON 2015
-------------------------------------------------------------------------------
-- File       : TTC_PON_TOP.vhd
-- Author     : Jubin MITRA (jmitra@cern.ch)
-- Company    : VECC
-- Created    : 17-03-2016
-- Last update: 17-03-2016
-- Platform   : 
-- Standard   : VHDL'93/08                  
-- Revision   : 1.0                                                     
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.1                                                                
-------------------------------------------------------------------------------
-- Description: 
-- It wraps up the Pattern Generator and Checker, PCS Layer and PHY Layer								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 17-03-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
--use ieee.std_logic_misc.all;

use work.TTC_PON_PACKAGE.all;
--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity TTC_PON_TOP is 
port (
-- CLOCKS
  		refclk_sfp_p                         : in  std_logic;
  		refclk1_p                            : in  std_logic;
        clk_50                               : in  std_logic;
        sma_clk_out                          : out std_logic;
        sma_tx_p                             : out std_logic;
        
-- 	SFP CONNECTIONS
        sfp_tx_p                             : out STD_LOGIC;
        sfp_rx_p                             : in  STD_LOGIC;
        sfp_tx_disable		                 : out STD_LOGIC;
        sfp_rs0				                 : out STD_LOGIC;
        sfp_rs1 				             : out STD_LOGIC;
        sfp_tx_fault		                 : in  STD_LOGIC;
        sfp_rx_los			                 : in  STD_LOGIC	        
);
end entity TTC_PON_TOP;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture mixed of TTC_PON_TOP is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
    signal GENERAL_RESET_I                          : std_logic := '0';    
    signal TX_PCS_RESET_I                           : std_logic := '0';
    signal RX_PCS_RESET_I                           : std_logic := '0';
    signal TX_PHY_RESET_I                           : std_logic := '0';
    signal RX_PHY_RESET_I                           : std_logic := '0';
    signal ATX_PLL_RESET_I                          : std_logic := '0';

    signal XCVR_REF_CLK_I                           : std_logic := '0';
    signal USER_LOGIC_CLK_I                         : std_logic := '0';
    signal TX_PMA_WORD_CLK                          : std_logic := '0';
    signal RX_PMA_WORD_CLK                          : std_logic := '0';
    signal CLOCK_ENABLE                             : std_logic := '0';
    
    
    -- PRBS DATA GENERATOR
    signal prbs_data_gen_mode_i 					: std_logic:='0';
    signal prbs_data_gen_enable_i					: std_logic:='0';
    signal prbs_data_gen_select_prbs_i				: std_logic_vector(1 downto 0):="00";
    signal prbs_data_gen_seed_i  					: std_logic_vector( 22 downto 0):=(others=>'0');
    signal prbs_data_gen_data_o            	        : std_logic_vector( 31 downto 0);
    
    -- PRBS DATA CHECKER
    signal prbs_data_check_enable_i                 : std_logic:='0';
    signal prbs_data_check_select_prbs_i            : std_logic_vector(1 downto 0):="00";
    signal prbs_data_check_lock_o    				: std_logic:='0';
    signal prbs_data_check_error_o					: std_logic:='0';
    signal prbs_data_check_data_i         	        : std_logic_vector( 31 downto 0);
    signal prbs_data_check_no_of_errors_o           : std_logic_vector( 7 downto 0);
    
    signal prbs_data_check_refgen_o        	        : std_logic_vector( 31 downto 0);
    signal rx_payload                               : std_logic_vector( 31 downto 0);
    signal rx_payload_counter                       : unsigned(  7 downto 0);
    signal rx_payload_valid                         : std_logic;
    
    --ERROR / BIT ACCUMULATORS
    signal err_acc                                  : unsigned(47 downto 0);
    signal bit_acc                                  : unsigned(47 downto 0);

    signal err_acc_latched                          : std_logic_vector(47 downto 0);
    signal bit_acc_latched                          : std_logic_vector(47 downto 0);
   
    signal clear_acc                                : std_logic;
    signal latch_acc                                : std_logic;
    
    -- Latency
    signal comma_k281_detect_flag                   : std_logic := '0';
    
    -- PCS LAYER
    signal ttc_pcs_i                                : pcs_signals_i;
    signal ttc_pcs_o                                : pcs_signals_o;
    
    signal cal_ena_i                                : std_logic;
    signal fast_beat_i                              : std_logic;
    signal slow_beat_i                              : std_logic;
    signal frame_ena_i                              : std_logic;
    signal user_data_tx                             : std_logic_vector(31 downto 0);
    signal onu_address_i                            : std_logic_vector( 7 downto 0);
    
    signal aligned_o                                : std_logic;
    signal reset_gtx_o                              : std_logic;
    signal rx_slide_o                               : std_logic;
    signal rx8b10ben_o                              : std_logic;
    signal comma_detect_o                           : std_logic;
    signal rx_k28_5_o                               : std_logic;
    signal rx_k28_1_o                               : std_logic;
    signal is_control_char_o                        : std_logic_vector(3 downto 0);
    signal user_data_tx_ready                       : std_logic;
    signal user_data_rx                             : std_logic_vector(31 downto 0);
   
    -- PHY LAYER
    signal ttc_phy_i                                : phy_signals_i;
    signal ttc_phy_o                                : phy_signals_o;
    
    signal tx_cal_busy_o                            : std_logic;
    signal rx_cal_busy_o                            : std_logic;
    signal rx_is_lockedtodata_o                     : std_logic;
    signal rx_is_lockedtoref_o                      : std_logic;
    signal pll_cal_busy_o                           : std_logic;
    signal pll_locked_o                             : std_logic;
    signal tx_ready_o                               : std_logic;
    signal rx_ready_o                               : std_logic;
    signal rx_seriallpbken_i                        : std_logic:='0';
    signal rx_pma_clkslip_i                         : std_logic:='0';
    
    -- PHASE CALC COMPONENT SIGNALS
    signal PHASE_COMP_RESET_I                       : std_logic;
    signal PHASE_SAMPLE_CLK                         : std_logic;
    signal PHASE_VALUE_INTEGER_PART       	        : std_logic_vector( 31 downto 0):=(others=>'0');
    signal PHASE_VALUE_FRACTIONAL_PART              : std_logic_vector( 31 downto 0):=(others=>'0');
    signal PHASE_VALUE_SIGN 				 		: std_logic := '0';
    signal PHASE_CALC_DONE 					  		: std_logic := '0';

    -- TEST SIGNALS-
    signal pattern1_check_bit                       : std_logic;
    signal pattern2_check_bit                       : std_logic;
    signal mux_sel_sma_out                          : std_logic_vector( 2 downto 0);
    signal sma_clk_o                                : std_logic;
    signal TX_PMA_WORD_CLK_FREQUENCY_VALUE          : std_logic_vector(31 downto 0);
    signal RX_PMA_WORD_CLK_FREQUENCY_VALUE          : std_logic_vector(31 downto 0);
    
    signal sources                                  : std_logic_vector(13 downto 0):=(others=>'0');
    signal probes                                   : std_logic_vector(385 downto 0);
 
    
    signal TICK                                     : std_logic_vector(4 downto 0);
    signal RXSHIFT, RXSAVED                         : std_logic_vector(239 downto 0);
    attribute keep: boolean;
    attribute keep of RXSHIFT: signal is true;
    attribute keep of RXSAVED: signal is true;
    attribute keep of prbs_data_check_refgen_o      : signal is true;

	--==================================== Component Declaration ====================================--          

    component prbs_data_gen  is
        generic (
            G_DOUT_WIDTH   : integer := 32;
            G_PRBS_TAP     : integer := 0;
            G_REVERSE_BITS : integer := 0);
        port (
            clock_i : in std_logic;
            mode_i  : in std_logic;  -- '0' external seed, '1' internal loop-back
            ena_i   : in std_logic;  -- '0' hold, '1' generate
            sel_i   : in std_logic_vector(1 downto 0);
            -- "00" PRBS-7, "01" PRBS-23
            -- "1x" fix
            seed_i  : in std_logic_vector(22 downto 0);
            dout_o  : out std_logic_vector(G_DOUT_WIDTH-1 downto 0));

    end component ;
        
    component prbs_data_check  is
        generic (
            G_DIN_WIDTH    : integer := 32;
            G_REVERSE_BITS : integer := 0);
        port (
            clock_i           : in  std_logic;
            reset_i           : in  std_logic;
            ena_i             : in  std_logic;
            sel_i             : in  std_logic_vector(1 downto 0);
            -- "00" PRBS-7, "01" PRBS-23
            -- "1x" fix
            din_i             : in  std_logic_vector(G_DIN_WIDTH-1 downto 0);
            lock_o            : out std_logic;
            error_o           : out std_logic;
            nerrs_o           : out std_logic_vector(7 downto 0);
            refgen_o          : out std_logic_vector(G_DIN_WIDTH-1 downto 0));

    end component ;

    component TTC_PCS_LAYER is 
    port (
    -- RESET
            GENERAL_RESET_I                      : in  std_logic;
            TX_PCS_RESET_I                       : in  std_logic;
            RX_PCS_RESET_I                       : in  std_logic;
            
    -- CLOCKS
            TX_PMA_WORD_CLK_I                    : in  std_logic;
            RX_PMA_WORD_CLK_I                    : in  std_logic;
            CLOCK_ENABLE_O                       : out std_logic;
            USER_CLK_I                           : in  std_logic;

    -- BUS LINES
            TTC_PCS_I                            : in  pcs_signals_i;
            TTC_PCS_O                            : out pcs_signals_o
            
    );
    end component;
    
    component TTC_PHY_LAYER is 
    port (
    -- RESET
            GENERAL_RESET_I                      : in std_logic;
            TX_PHY_RESET_I                       : in std_logic;
            RX_PHY_RESET_I                       : in std_logic;
            ATX_PLL_RESET_I                      : in std_logic;
            
    -- CLOCKS
            XCVR_REF_CLK_I                       : in  std_logic;
            USER_LOGIC_CLK_I                     : in  std_logic;
            TX_PMA_WORD_CLK_O                    : out std_logic;
            RX_PMA_WORD_CLK_O                    : out std_logic;
            
    -- User Data Bus Lines
            XCVR_PHY_I                           : in  phy_signals_i;
            XCVR_PHY_O                           : out phy_signals_o
    );
    end component;

    component IOPLL is
	port (
		outclk_0 : out std_logic;        
		refclk   : in  std_logic := '0'; 
		rst      : in  std_logic := '0'  
	);
    end component;
    
    component phase_sample_clk_pll is
		port (
			rst      : in  std_logic := 'X'; -- reset
			refclk   : in  std_logic := 'X'; -- clk
			locked   : out std_logic;        -- export
			outclk_0 : out std_logic         -- clk
		);
	end component phase_sample_clk_pll;


    component FPLL_CASCADE_SOURCE is
	port (
		hssi_pll_cascade_clk : out std_logic;
		pll_cal_busy         : out std_logic;
		pll_locked           : out std_logic;
		pll_powerdown        : in  std_logic := '0';
		pll_refclk0          : in  std_logic := '0'
	);
    end component;

    component ISSP is
	port (
		probe      : in  std_logic_vector(385 downto 0) := (others => '0'); 
        source_clk : in std_logic;
		source     : out std_logic_vector(13 downto 0)                      
	);
    end component;
    
    
   component FrequencyCalculator is
   generic (
		constant REFERENCE_FREQUENCY_VALUE    			: integer:= 100
    );
    port (
      RESET_I                                    : in  std_logic;
      CLK_REF_I   		                         : in  std_logic; 
      CLK_UNKNOWN_I   		                     : in  std_logic; 
      CLK_FREQUENCY_VALUE_O						 : out std_logic_vector(31 downto 0);
      COMPUTATION_DONE_O                         : out std_logic 
    );
    end component;

--    component temp_measure_ip is
--		port (
--			corectl : in  std_logic                    := 'X'; 
--			reset   : in  std_logic                    := 'X'; 
--			tempout : out std_logic_vector(9 downto 0);        
--			eoc     : out std_logic                            
--		);
--	end component;
    
    component phase_calc is
    generic (
        constant NUMBER_OF_CYCLES			      : std_logic_vector( 31 downto 0) := X"1000_0000";
        constant CALIBRATION_FACTOR				  : integer						 := 360
    );
    port (
        RESET_I                                   : in  std_logic;
        REF_CLK_I                                 : in  std_logic; 
        PHASE_SHIFTED_CLK_I                       : in  std_logic; 
        SAMPLE_CLK_I						      : in  std_logic;
        PHASE_VALUE_INTEGER_PART_O                : out std_logic_vector( 31 downto 0);
        PHASE_VALUE_FRACTIONAL_PART_O             : out std_logic_vector( 31 downto 0);
        PHASE_VALUE_SIGN_O						  : out std_logic;
        PHASE_CALC_DONE_O						  : out std_logic    
    );
    end component;
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
 
   --==================================== Port Mapping ==============================================--  
    prbs_data_gen_comp:
    prbs_data_gen
        generic map (
            G_DOUT_WIDTH   => 32,
            G_PRBS_TAP     => 0,
            G_REVERSE_BITS => 0)
        port map(
            clock_i        => USER_LOGIC_CLK_I,
            mode_i         => prbs_data_gen_mode_i,        -- '0' external seed, '1' internal loop-back
            ena_i          => prbs_data_gen_enable_i,      -- '0' hold, '1' generate
            sel_i          => prbs_data_gen_select_prbs_i, -- "00" PRBS-7, "01" PRBS-23, "1x" fix
            seed_i         => prbs_data_gen_seed_i,
            dout_o         => prbs_data_gen_data_o
            );

    prbs_data_check_comp:
    prbs_data_check 
        generic map (
            G_DIN_WIDTH       => 32,
            G_REVERSE_BITS    => 0)
        port map (
            clock_i           => RX_PMA_WORD_CLK,
            reset_i           => GENERAL_RESET_I,
            ena_i             => prbs_data_check_enable_i,        -- '0' hold, '1' generate
            sel_i             => prbs_data_check_select_prbs_i,   -- "00" PRBS-7, "01" PRBS-23, "1x" fix
            din_i             => prbs_data_check_data_i,
            lock_o            => prbs_data_check_lock_o,
            error_o           => prbs_data_check_error_o,
            nerrs_o           => prbs_data_check_no_of_errors_o,
            refgen_o          => prbs_data_check_refgen_o
            );

    TTC_PCS_LAYER_comp:
    TTC_PCS_LAYER  
    port map (
    -- RESET
            GENERAL_RESET_I                      => GENERAL_RESET_I,
            TX_PCS_RESET_I                       => TX_PCS_RESET_I,
            RX_PCS_RESET_I                       => RX_PCS_RESET_I,
            
    -- CLOCKS
            TX_PMA_WORD_CLK_I                    => TX_PMA_WORD_CLK,
            RX_PMA_WORD_CLK_I                    => RX_PMA_WORD_CLK,
            CLOCK_ENABLE_O                       => CLOCK_ENABLE,
            USER_CLK_I                           => USER_LOGIC_CLK_I,

    -- BUS LINES
            TTC_PCS_I                            => ttc_pcs_i, 
            TTC_PCS_O                            => ttc_pcs_o
            
    );
    
    TTC_PHY_LAYER_comp:
    TTC_PHY_LAYER 
    port map (
    -- RESET
            GENERAL_RESET_I                      => GENERAL_RESET_I,
            TX_PHY_RESET_I                       => TX_PHY_RESET_I,
            RX_PHY_RESET_I                       => RX_PHY_RESET_I,
            ATX_PLL_RESET_I                      => ATX_PLL_RESET_I,
            
    -- CLOCKS
            XCVR_REF_CLK_I                       => XCVR_REF_CLK_I,
            USER_LOGIC_CLK_I                     => USER_LOGIC_CLK_I,
            TX_PMA_WORD_CLK_O                    => TX_PMA_WORD_CLK,
            RX_PMA_WORD_CLK_O                    => RX_PMA_WORD_CLK,
            
    -- User Data Bus Lines
            XCVR_PHY_I                           => ttc_phy_i,
            XCVR_PHY_O                           => ttc_phy_o
    );

    ISSP_COMP:
    ISSP
	port map (
		probe       => probes,
        source_clk  => TX_PMA_WORD_CLK,
		source      => sources                     
	);
    
    phase_sample_clk_pll_comp:
    phase_sample_clk_pll  
	port map (
		rst                                 => GENERAL_RESET_I,
		refclk                              => refclk_sfp_p,
		outclk_0                            => PHASE_SAMPLE_CLK,     
		locked                              => open     
	);


    -- FPLL_CASCADE_SOURCE_comp:
    -- FPLL_CASCADE_SOURCE
	-- port map (
		-- hssi_pll_cascade_clk                    => XCVR_REF_CLK_I,
		-- pll_powerdown                           => GENERAL_RESET_I,
		-- pll_refclk0                             => refclk_sfp_p
	-- );

    IOPLL_comp:
    IOPLL 
	port map(
		outclk_0                                => USER_LOGIC_CLK_I,
		refclk                                  => clk_50,
		rst                                     => GENERAL_RESET_I
	);
    
    TX_PMA_WORD_CLK_FrequencyCalculator_comp:
    FrequencyCalculator
    generic map (
  		REFERENCE_FREQUENCY_VALUE    			=> 125
     )
     port map (
       RESET_I                                  => GENERAL_RESET_I,
       CLK_REF_I   		                        => USER_LOGIC_CLK_I, 
       CLK_UNKNOWN_I   		                    => TX_PMA_WORD_CLK,
       CLK_FREQUENCY_VALUE_O					=> TX_PMA_WORD_CLK_FREQUENCY_VALUE,
       COMPUTATION_DONE_O                       => open 
     );

    RX_PMA_WORD_CLK_FrequencyCalculator_comp:
    FrequencyCalculator
    generic map (
  		REFERENCE_FREQUENCY_VALUE    			=> 125
     )
     port map (
       RESET_I                                  => GENERAL_RESET_I,
       CLK_REF_I   		                        => USER_LOGIC_CLK_I, 
       CLK_UNKNOWN_I   		                    => RX_PMA_WORD_CLK,
       CLK_FREQUENCY_VALUE_O					=> RX_PMA_WORD_CLK_FREQUENCY_VALUE,
       COMPUTATION_DONE_O                       => open 
     );
     
    phase_calc_comp:
    phase_calc
    generic map(
	    NUMBER_OF_CYCLES					    => x"0010_0000",
	    CALIBRATION_FACTOR						=> 4166
    )
    port map (
       RESET_I 		                            => PHASE_COMP_RESET_I,
       REF_CLK_I		                        => refclk_sfp_p,	  	  
       PHASE_SHIFTED_CLK_I	                    => sma_clk_o,
	   SAMPLE_CLK_I							    => PHASE_SAMPLE_CLK,
    
       PHASE_VALUE_INTEGER_PART_O               => PHASE_VALUE_INTEGER_PART,
       PHASE_VALUE_FRACTIONAL_PART_O            => PHASE_VALUE_FRACTIONAL_PART,
	   PHASE_VALUE_SIGN_O						=> PHASE_VALUE_SIGN,
	   PHASE_CALC_DONE_O					    => PHASE_CALC_DONE
    );
   --=================================================================================================================--
   --=================================== DATA and CONTROL SIGNAL CONNECTIONS =========================================--
   --=================================================================================================================--


                              
    -- ============================ --
    -- PHY -- PCS LAYER INTERFACING --
    -- ============================ --
    
    ttc_phy_i.tx_parallel_data_i                     <= ttc_pcs_o.tx_parallel_data_o;   
    --ttc_phy_i.tx_parallel_data_i                     <= x"A0C0FF00FF";   
    ttc_pcs_i.rx_parallel_data_i                     <= ttc_phy_o.rx_parallel_data_o;
    
    ttc_phy_i.rx_serial_data_i                       <= sfp_rx_p;
    sfp_tx_p                                         <= ttc_phy_o.tx_serial_data_o;
    
    ttc_phy_i.rx_seriallpbken_i                      <= rx_seriallpbken_i;
    ttc_phy_i.rx_pma_clkslip_i                       <= rx_pma_clkslip_i or ttc_pcs_o.bitslip_o;
    
    tx_cal_busy_o                                    <= ttc_phy_o.tx_cal_busy_o;          
    rx_cal_busy_o                                    <= ttc_phy_o.rx_cal_busy_o;
    rx_is_lockedtodata_o                             <= ttc_phy_o.rx_is_lockedtodata_o;
    rx_is_lockedtoref_o                              <= ttc_phy_o.rx_is_lockedtoref_o;
    pll_cal_busy_o                                   <= ttc_phy_o.pll_cal_busy_o;
    pll_locked_o                                     <= ttc_phy_o.pll_locked_o;
    tx_ready_o                                       <= ttc_phy_o.tx_ready_o;
    rx_ready_o                                       <= ttc_phy_o.rx_ready_o;
    
    --rx_seriallpbken_i                                <= '0';

    ttc_pcs_i.rx_phy_ready_i                         <= rx_ready_o;

    -- ========================== --
    -- PCS -- DATA LINK INTERFACE --
    -- ========================== --
    -- PCS
        ttc_pcs_i.cal_ena_i                           <= cal_ena_i;        
        ttc_pcs_i.fast_beat_i                         <= fast_beat_i;                                   
        ttc_pcs_i.slow_beat_i                         <= slow_beat_i;                        
        
        ttc_pcs_i.frame_ena_i                         <= frame_ena_i;                        
        
        ttc_pcs_i.tx_data_payload_i                   <= user_data_tx;
        ttc_pcs_i.onu_address_i                       <= onu_address_i;
        user_data_tx_ready                            <= ttc_pcs_o.tx_ready_o;                     
                
        user_data_rx                                  <= ttc_pcs_o.rx_data_payload_o;

        aligned_o                                     <= ttc_pcs_o.aligned_o;
        reset_gtx_o                                   <= ttc_pcs_o.reset_gtx_o;
        
        is_control_char_o                             <= ttc_pcs_o.is_control_char_o; 
        
        ttc_pcs_i.tx_frame_i                          <= x"BC12_3456_1111_1111_2222_2222_3333_3333_4444_4444_5555_5555";
 
      -- -- PRBS DATA GENERATOR
        -- prbs_data_gen_mode_i 					      <= not GENERAL_RESET_I;
        -- prbs_data_gen_enable_i					      <= user_data_tx_ready;
        -- prbs_data_gen_select_prbs_i				      <="00";
        -- prbs_data_gen_seed_i  					      <= "111" & x"F_FFFF";
        -- user_data_tx                                  <= prbs_data_gen_data_o;
    
    -- PRBS DATA CHECKER
         prbs_data_check_enable_i                      <= rx_payload_valid;
         prbs_data_check_select_prbs_i                 <="00";
         prbs_data_check_data_i         	           <= rx_payload;
    
    -- -- ONU ADDRESS
        -- onu_address_i                                 <= "10101110";
    --prbs_data_check_lock_o    				        
    --prbs_data_check_error_o					        
    --prbs_data_check_no_of_errors_o           
    --prbs_data_check_refgen_o        

    -- ========================== --
    -- CALIBRATION LOGIC --
    -- ========================== --   
        cal_ena_i                                     <= '0';
        fast_beat_i                                   <= '0';
        slow_beat_i                                   <= '0';
        frame_ena_i                                   <= '1';

    -- ========================== --
    -- RECONFIG CONNECTIONS --
    -- ========================== --   
    ttc_phy_i.ATX_PLL_reconfig_clk_i                  <= USER_LOGIC_CLK_I;
    ttc_phy_i.TX_RX_PHY_reconfig_clk_i                <= USER_LOGIC_CLK_I;
    ttc_phy_i.ATX_PLL_reconfig_reset_i                <= GENERAL_RESET_I;
    ttc_phy_i.TX_RX_PHY_reconfig_reset_i              <= GENERAL_RESET_I;
    

    -- ============================================================================ --
    -- -------------------------    BER Checker ----------------------------------- --
    -- ============================================================================ --
    p_payload_extract: process(RX_PMA_WORD_CLK) is
    begin
        if (RX_PMA_WORD_CLK'event and RX_PMA_WORD_CLK = '1') then
            if (is_control_char_o(0) = '1' and ttc_pcs_o.rx_data_payload_o(7 downto 0) = X"BC") or
               (is_control_char_o(0) = '1' and ttc_pcs_o.rx_data_payload_o(7 downto 0) = X"3C") then
                rx_payload_counter <= x"05";
            elsif rx_payload_counter > 0 then
                rx_payload_counter <= rx_payload_counter - 1;
            elsif rx_payload_counter = 0 then
                rx_payload_counter <= x"05";
            end if;
            rx_payload_valid <= '0';
            if rx_payload_counter > 0 then
                rx_payload <= ttc_pcs_o.rx_data_payload_o;
                rx_payload_valid <= '1';
            end if;
        end if;
    end process p_payload_extract;
    
    p_acc : process(RX_PMA_WORD_CLK) is
    begin
        if(RX_PMA_WORD_CLK'event and RX_PMA_WORD_CLK='1') then
            if(clear_acc='1') then
                 err_acc <= to_unsigned(0,err_acc'length);
                 bit_acc <= to_unsigned(0,bit_acc'length);
                 err_acc_latched <= (others => '0');
                 bit_acc_latched <= (others => '0');
            else
                 err_acc<=err_acc+unsigned(prbs_data_check_no_of_errors_o);
                
                 if(rx_payload_valid='1') then
                                 bit_acc<=bit_acc+to_unsigned(32,bit_acc'length);
                 else
                                 bit_acc<=bit_acc;
                 end if;
                
                 if(latch_acc='1') then
                                 err_acc_latched<=std_logic_vector(err_acc);
                                 bit_acc_latched<=std_logic_vector(bit_acc);
                 end if;
                        
            end if;
        end if;
    end process p_acc;        
        
        k28_1_detect:
        process(RX_PMA_WORD_CLK)
        begin
            if rising_edge(RX_PMA_WORD_CLK) then
                if is_control_char_o(0) = '1' and ttc_pcs_o.rx_data_payload_o(7 downto 0) = X"3C" then
                    comma_k281_detect_flag              <= '1';
                else
                    comma_k281_detect_flag              <= '0';
                end if;
            end if;
        end process;
               
    
    -- ============================================================================= --
    -- -------------------------In System Signals and Probes ----------------------- --
    -- ============================================================================= ==
   
    GENERAL_RESET_I                                  <= sources(0);
    TX_PHY_RESET_I                                   <= sources(1);
    TX_PCS_RESET_I                                   <= sources(1);
    RX_PHY_RESET_I                                   <= sources(2) or ttc_pcs_o.reset_gtx_o;
    RX_PCS_RESET_I                                   <= sources(2);
    ATX_PLL_RESET_I                                  <= sources(3);
    rx_pma_clkslip_i                                 <= sources(4);
    rx_seriallpbken_i                                <= sources(5);
    mux_sel_sma_out                                  <= sources(8 downto 6);
    PHASE_COMP_RESET_I                               <= sources(9);
    ttc_pcs_i.onu_word_aligner_enable_i              <= sources(10);
	sfp_tx_disable		                             <= sources(11);	
    clear_acc                                        <= sources(12);
    latch_acc                                        <= sources(13);
    
    probes(0)                                        <= ttc_phy_o.tx_cal_busy_o;          
    probes(1)                                        <= ttc_phy_o.rx_cal_busy_o;
    probes(2)                                        <= ttc_phy_o.rx_is_lockedtodata_o;
    probes(3)                                        <= ttc_phy_o.rx_is_lockedtoref_o;
    probes(4)                                        <= ttc_phy_o.pll_cal_busy_o;
    probes(5)                                        <= ttc_phy_o.pll_locked_o;
    probes(6)                                        <= ttc_phy_o.tx_ready_o;
    probes(7)                                        <= ttc_phy_o.rx_ready_o;
    
    process(RX_PMA_WORD_CLK)
    begin
        if rising_edge(RX_PMA_WORD_CLK) then
            if mux_sel_sma_out="011" then
                probes(47 downto 8)                              <= ttc_phy_o.rx_parallel_data_o;
            else
                probes(47 downto 8)                              <= x"00"& ttc_pcs_o.rx_data_payload_o;
            end if;
        end if;
    end process;


    -- process(RX_PMA_WORD_CLK)
    -- begin
        -- if rising_edge(RX_PMA_WORD_CLK) then
                -- RXSHIFT <= ttc_phy_o.rx_parallel_data_o & RXSHIFT(239 downto 40);

                -- TICK <= not or_reduce(TICK) & TICK(4 downto 1);
                
                -- if TICK = "00000" then
                        -- RXSAVED <= RXSHIFT;
                -- end if;
        -- end if;
    -- end process;



    probes( 79 downto  48)                           <= TX_PMA_WORD_CLK_FREQUENCY_VALUE;                                                  
    probes(111 downto  80)                           <= RX_PMA_WORD_CLK_FREQUENCY_VALUE;     
    probes(143 downto 112)                           <= PHASE_VALUE_INTEGER_PART;
    probes(144)                                      <= PHASE_VALUE_SIGN;
    probes(145)                                      <= PHASE_CALC_DONE;
    probes(149 downto 146)                           <= ttc_pcs_o.word_aligner_state_o;
    probes(157 downto 150)                           <= ttc_pcs_o.bitslip_num_o;
    --probes(349 downto 158)                           <= ttc_pcs_o.rx_frame_o;
    probes(158)                                      <= prbs_data_check_lock_o;
    probes(159)                                      <= prbs_data_check_error_o;
    probes(167 downto 160)                           <= prbs_data_check_no_of_errors_o;
    probes(199 downto 168)                           <= prbs_data_check_refgen_o;
    probes(247 downto 200)                           <= err_acc_latched;
    probes(295 downto 248)                           <= bit_acc_latched;
    
    -- ============================================================================= --
    -- -------------------------TOP LEVEL CONNECTIONS ------------------------------ --
    -- ============================================================================= ==
    XCVR_REF_CLK_I                                   <= refclk_sfp_p;
    --USER_LOGIC_CLK_I                                 <= clk_50;
    
    pattern_check_proc:
    process(RX_PMA_WORD_CLK)
    begin
        if rising_edge(RX_PMA_WORD_CLK) then
            if   ttc_phy_o.rx_parallel_data_o= x"FFFFE00001" then
                pattern1_check_bit          <= '1';
            else
                pattern1_check_bit          <= '0';
            end if;
            
            if   ttc_phy_o.rx_parallel_data_o= x"FFFFC00001" then
                pattern2_check_bit          <= '1';
            else 
                pattern2_check_bit          <= '0';
            end if; 
        end if;
    end process;
    
    -- sma_clk_o                                       <=    pattern1_check_bit when mux_sel_sma_out="000"
                                                    -- else   pattern2_check_bit when mux_sel_sma_out="001"
                                                    -- else   TX_PMA_WORD_CLK when mux_sel_sma_out="010"
                                                    -- else   RX_PMA_WORD_CLK;
                                                    
    sma_clk_o                                        <= comma_k281_detect_flag when mux_sel_sma_out="000"
                                                        else RX_PMA_WORD_CLK;
    
    sma_clk_out                                      <= sma_clk_o;
    sfp_rs0				                             <= '1';
	sfp_rs1				                             <= '1';

end architecture mixed;