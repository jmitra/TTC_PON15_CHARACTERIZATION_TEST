library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--==============================================================================
--! Entity declaration for PRBS generator
--==============================================================================
entity prbs_data_gen  is
    generic (
        G_DOUT_WIDTH   : integer := 20;
        G_PRBS_TAP     : integer := 0;
        G_REVERSE_BITS : integer := 0);
    port (
        clock_i : in std_logic;
        mode_i  : in std_logic;  -- '0' external seed, '1' internal loop-back
        ena_i   : in std_logic;  -- '0' hold, '1' generate
        sel_i   : in std_logic_vector(1 downto 0);
        -- "00" PRBS-7, "01" PRBS-23
        -- "1x" fix
        seed_i  : in std_logic_vector(22 downto 0);
        dout_o  : out std_logic_vector(G_DOUT_WIDTH-1 downto 0));

end entity prbs_data_gen ;

--==============================================================================
-- architecture declaration
--==============================================================================
architecture RTL of prbs_data_gen is

    function slv_bitreverse (slv : in std_logic_vector) return std_logic_vector is
        variable slv_ret : std_logic_vector(slv'RANGE);
        alias slv_rr     : std_logic_vector(slv'REVERSE_RANGE) is slv;
    begin
        for i in slv'RANGE loop
            slv_ret(i) := slv_rr(i);
        end loop;
        return slv_ret;
    end function slv_bitreverse;

    function f_lfsr7 (seed : std_logic_vector(6 downto 0)) return std_logic_vector is
        variable result : std_logic_vector(6 downto 0);
    begin  -- function f_lfsr
        result := seed(5 downto 0) & (seed(6) xor seed(5));
        return result;
    end function f_lfsr7;

    function f_lfsr23 (seed : std_logic_vector(22 downto 0)) return std_logic_vector is
        variable result : std_logic_vector(22 downto 0);
    begin  -- function f_lfsr
        result := seed(21 downto 0) & (seed(22) xor seed(17));
        return result;
    end function f_lfsr23;

    signal prbs7_seed  : std_logic_vector(6 downto 0);
    signal prbs7_bits  : std_logic_vector(G_DOUT_WIDTH*7-1 downto 0);
    signal prbs7_dout  : std_logic_vector(G_DOUT_WIDTH-1 downto 0);
    signal prbs23_seed : std_logic_vector(22 downto 0);
    signal prbs23_bits : std_logic_vector(G_DOUT_WIDTH*23-1 downto 0);
    signal prbs23_dout : std_logic_vector(G_DOUT_WIDTH-1 downto 0);
--==============================================================================
-- architecture begin
--==============================================================================
begin  -- architecture RTL

    gen_prbs7_bits: for i in 0 to G_DOUT_WIDTH-1 generate
        gen_0: if i=0 generate
            prbs7_bits(i*7+6 downto i*7) <= f_lfsr7(prbs7_seed);
        end generate gen_0;
        gen_N: if i>0 generate
            prbs7_bits(i*7+6 downto i*7) <= f_lfsr7(prbs7_bits((i-1)*7+6 downto (i-1)*7));
        end generate gen_N;
    end generate gen_prbs7_bits;
    
    p_prbs7_fb: process(clock_i) is
    begin
        if clock_i'event and clock_i = '1' then
            if mode_i = '0' then
                prbs7_seed <= seed_i(prbs7_seed'range);
            elsif ena_i = '0' then
                prbs7_seed <= prbs7_seed;
            else
                prbs7_seed <= prbs7_bits(7*(G_DOUT_WIDTH-1)+6 downto 7*(G_DOUT_WIDTH-1));
            end if;
        end if;
    end process p_prbs7_fb;

    p_prbs7_reg: process(clock_i) is
    begin
        if clock_i'event and clock_i = '1' then
            for i in 0 to G_DOUT_WIDTH-1 loop
                prbs7_dout(i) <= prbs7_bits(7*(G_DOUT_WIDTH-i-1)+G_PRBS_TAP);
            end loop;
        end if;
    end process p_prbs7_reg;

    gen_prbs23_bits: for i in 0 to G_DOUT_WIDTH-1 generate
        gen_0: if i=0 generate
            prbs23_bits(i*23+22 downto i*23) <= f_lfsr23(prbs23_seed);
        end generate gen_0;
        gen_N: if i>0 generate
            prbs23_bits(i*23+22 downto i*23) <= f_lfsr23(prbs23_bits((i-1)*23+22 downto (i-1)*23));
        end generate gen_N;
    end generate gen_prbs23_bits;

    p_prbs23_fb: process(clock_i) is
    begin
        if clock_i'event and clock_i = '1' then
            if mode_i = '0' then
                prbs23_seed <= not seed_i(prbs23_seed'range);
            elsif ena_i = '0' then
                prbs23_seed <= prbs23_seed;
            else
                prbs23_seed <= prbs23_bits(23*(G_DOUT_WIDTH-1)+22 downto 23*(G_DOUT_WIDTH-1));
            end if;
        end if;
    end process p_prbs23_fb;

    p_prbs23_reg: process(clock_i) is
    begin
        if clock_i'event and clock_i = '1' then
            for i in 0 to G_DOUT_WIDTH-1 loop
                prbs23_dout(i) <= not prbs23_bits(23*(G_DOUT_WIDTH-i-1)+G_PRBS_TAP);
            end loop;
        end if;
    end process p_prbs23_reg;
    
    no_reverse : if G_REVERSE_BITS = 0 generate
        dout_o <= prbs7_dout when sel_i = "00" else
                  prbs23_dout when sel_i = "01" else
                  (others => '1');
    end generate no_reverse;

    reverse : if G_REVERSE_BITS = 1 generate
        dout_o <= slv_bitreverse(prbs7_dout) when sel_i = "00" else
                  slv_bitreverse(prbs23_dout) when sel_i = "01" else
                  (others => '1');
    end generate reverse;

end architecture RTL;
--==============================================================================
-- architecture end
--==============================================================================
