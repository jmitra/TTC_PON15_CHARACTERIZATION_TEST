--=================================================================================================--
--##################################   Package Information   ######################################--
--=================================================================================================--
-------------------------------------------------------------------------------
-- Title      : PHYSICAL LAYER CONNECTIONS 
-- Project    : TTC-PON 2015
-------------------------------------------------------------------------------
-- File       : TTC_PON_PACKAGE.vhd
-- Author     : Jubin MITRA (jmitra@cern.ch)
-- Company    : VECC
-- Created    : 14-03-2016
-- Last update: 14-03-2016
-- Platform   : 
-- Standard   : VHDL'93/08                  
-- Revision   : 1.0                                                     
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.1                                                                
-------------------------------------------------------------------------------
-- Description: 
-- This package contains all the bus signals								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author      Description
-- 14-03-2016  1.0      Jubin Mitra	    Created
-------------------------------------------------------------------------------
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--=================================================================================================--
--##################################   Package Declaration   ######################################--
--=================================================================================================--

package TTC_PON_PACKAGE is
   
   --=================================== CONSTANT DECLARATIONS ==================================--
 
   --=====================================================================================--
   --=====================================================================================--
   
   --================================ Record Declarations ================================--   
   
   --====================--
     type phy_signals_i is
     record
     -- Data Signals
		tx_parallel_data_i                   : std_logic_vector(39 downto 0); 
                                             
     -- Serial Data                          
		rx_serial_data_i                     : std_logic; 
                                             
     -- Control Signals                      
		rx_seriallpbken_i                    : std_logic; 

     -- RX_PMA_CLK_SLIP 
        rx_pma_clkslip_i                     : std_logic;
        
    -- Avalon-MM Signals RX_PHY              
        TX_RX_PHY_reconfig_write_i           : std_logic;
		TX_RX_PHY_reconfig_read_i            : std_logic;
		TX_RX_PHY_reconfig_address_i         : std_logic_vector(9 downto 0);
		TX_RX_PHY_reconfig_writedata_i       : std_logic_vector(31 downto 0);
		TX_RX_PHY_reconfig_clk_i             : std_logic;
		TX_RX_PHY_reconfig_reset_i           : std_logic;
                                             
    -- Avalon-MM Signals ATX_PLL             
        ATX_PLL_reconfig_write_i             : std_logic;
		ATX_PLL_reconfig_read_i              : std_logic;
		ATX_PLL_reconfig_address_i           : std_logic_vector(9 downto 0);
		ATX_PLL_reconfig_writedata_i         : std_logic_vector(31 downto 0);
		ATX_PLL_reconfig_clk_i               : std_logic;
		ATX_PLL_reconfig_reset_i             : std_logic;

    end record;     

     type phy_signals_o is
     record
     -- Data Signals
		rx_parallel_data_o                  : std_logic_vector(39 downto 0);                    
                                            
     -- Serial Data                         
        tx_serial_data_o                    : std_logic;                     
                                            
     -- Monitor Signals                     
        tx_cal_busy_o                       : std_logic;
        rx_cal_busy_o                       : std_logic;
        rx_is_lockedtodata_o                : std_logic;
        rx_is_lockedtoref_o                 : std_logic;
        pll_cal_busy_o                      : std_logic;
        pll_locked_o                        : std_logic;
        tx_ready_o                          : std_logic;
        rx_ready_o                          : std_logic;
        
     -- Avalon-MM Signals RX_PHY
		TX_RX_PHY_reconfig_readdata_o       : std_logic_vector(31 downto 0); 
		TX_RX_PHY_reconfig_waitrequest_o    : std_logic;
        
     -- Avalon-MM Signals TX_ATX_PLL
		ATX_PLL_reconfig_readdata_o         : std_logic_vector(31 downto 0); 
		ATX_PLL_reconfig_waitrequest_o      : std_logic;
 
    end record;   
    
    type pcs_signals_i is
    record
        cal_ena_i                           : std_logic;
        fast_beat_i                         : std_logic;                     
        slow_beat_i                         : std_logic;
        
        frame_ena_i                         : std_logic;
        
        tx_frame_i                          : std_logic_vector(191 downto 0);
        tx_data_payload_i                   : std_logic_vector(31 downto 0);
        onu_address_i                       : std_logic_vector( 7 downto 0);
        
        rx_parallel_data_i                  : std_logic_vector(39 downto 0);
        
        rx_phy_ready_i                      : std_logic;
        
        onu_word_aligner_enable_i           : std_logic;
    end record;
 
    type pcs_signals_o is
    record
         tx_ready_o                         : std_logic;                     
         
         tx_parallel_data_o                 : std_logic_vector(39 downto 0);

         rx_frame_o                         : std_logic_vector(191 downto 0);         
         rx_data_payload_o                  : std_logic_vector(31 downto 0);

         aligned_o                          : std_logic;
         reset_gtx_o                        : std_logic;         
         bitslip_o                          : std_logic;
         bitslip_num_o                      : std_logic_vector(7 downto 0);
         word_aligner_state_o               : std_logic_vector(3 downto 0);
         
         is_control_char_o                  : std_logic_vector(3 downto 0);
    end record; 
   --=====================================================================================--   
end TTC_PON_PACKAGE;   
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--