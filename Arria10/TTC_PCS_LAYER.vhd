--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
-------------------------------------------------------------------------------
-- Title      : PHYSICAL CODING SUBLAYER CONNECTIONS 
-- Project    : TTC-PON 2015
-------------------------------------------------------------------------------
-- File       : TTC_PCS_LAYER.vhd
-- Author     : Jubin MITRA (jmitra@cern.ch)
-- Company    : VECC
-- Created    : 16-03-2016
-- Last update: 16-03-2016
-- Platform   : 
-- Standard   : VHDL'93/08                  
-- Revision   : 1.0                                                     
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.1                                                                
-------------------------------------------------------------------------------
-- Description: 
-- It connects all the modules related to Physical Coding Sublayer								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 16-03-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.TTC_PON_PACKAGE.all;
--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity TTC_PCS_LAYER is 
port (
-- RESET
        GENERAL_RESET_I                      : in  std_logic;
        TX_PCS_RESET_I                       : in  std_logic;
        RX_PCS_RESET_I                       : in  std_logic;
        
-- CLOCKS
        TX_PMA_WORD_CLK_I                    : in  std_logic;
        RX_PMA_WORD_CLK_I                    : in  std_logic;
        CLOCK_ENABLE_O                       : out std_logic;
        USER_CLK_I                           : in  std_logic;

-- BUS LINES
        TTC_PCS_I                            : in  pcs_signals_i;
        TTC_PCS_O                            : out pcs_signals_o
        
);
end entity TTC_PCS_LAYER;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture mixed of TTC_PCS_LAYER is

	--====================================== Constant Definition ====================================--   
	

	--====================================== Signal Definition ======================================--   
    
    signal TX_RESET                                         : std_logic;
    signal RX_RESET                                         : std_logic;
    signal clock_enable                                     : std_logic;
    
    --8b10 Encoder Decoder
    signal enc_8b10b_data_i                                 : std_logic_vector(31 downto 0);
    signal enc_8b10b_data_o                                 : std_logic_vector(39 downto 0);
    signal enc_8b10b_is_control_char_i                      : std_logic_vector( 3 downto 0);
    signal dec_8b10b_data_i                                 : std_logic_vector(39 downto 0);
    signal dec_8b10b_data_o                                 : std_logic_vector(31 downto 0);
    signal dec_8b10b_data_o_byte_flip                       : std_logic_vector(31 downto 0);
    signal dec_8b10b_is_control_char_o                      : std_logic_vector( 3 downto 0);
    
    -- ONU FRAME GENERATOR
    signal onu_frame_gen_cal_ena_i                          : std_logic;
    signal onu_frame_gen_fast_beat_i                        : std_logic;
    signal onu_frame_gen_slow_beat_i                        : std_logic;
    signal onu_frame_gen_frame_ena_i                        : std_logic;
    signal onu_frame_gen_tx_data_i                          : std_logic_vector(31 downto 0);
    signal onu_frame_gen_address_i                          : std_logic_vector( 7 downto 0);
    signal onu_frame_gen_ready_o                            : std_logic;
    signal onu_frame_gen_txdata_o                           : std_logic_vector(31 downto 0);
    signal onu_frame_gen_tx8b10bypass_o                     : std_logic_vector( 3 downto 0);
    signal onu_frame_gen_txcharisk_o                        : std_logic_vector( 3 downto 0);
    
    signal tx_frame_to_word_gen_counter                     : std_logic_vector( 3 downto 0);
    signal rx_word_to_frame_gen_counter                     : std_logic_vector( 3 downto 0);
    
    -- ONU WORD ALIGNER
    signal onu_word_aligner_enable_i                        : std_logic;
    signal onu_word_aligner_rxdata_i                        : std_logic_vector(39 downto 0);
    signal onu_word_aligner_bitslip_num_o                   : std_logic_vector( 7 downto 0);
    signal onu_word_aligner_aligned_o                       : std_logic;
    signal onu_word_aligner_reset_gtx_o                     : std_logic;
    signal onu_word_aligner_bitslip_o                       : std_logic;
    signal onu_word_aligner_state_o                         : std_logic_vector( 3 downto 0);

    signal rx_frame_o_buf                                   : std_logic_vector(191 downto 0);
    
    
    -- BARREL SHIFTER
    signal barrel_shift_din_i                               : std_logic_vector(39 downto 0);
    signal barrel_shift_nshift_i                            : std_logic_vector( 5 downto 0);
    signal barrel_shift_dout_o                              : std_logic_vector(39 downto 0);

    -- INTERNAL SIGNALS
    signal tx_parallel_data                                 : std_logic_vector(39 downto 0);

    --==================================== Component Declaration ====================================--          

    -- 8b10b Encoder Decoder
    component enc_8b10b
	port(
		RESET : in std_logic;
		SBYTECLK : in std_logic;
		KI : in std_logic;
		AI : in std_logic;
		BI : in std_logic;
		CI : in std_logic;
		DI : in std_logic;
		EI : in std_logic;
		FI : in std_logic;
		GI : in std_logic;
		HI : in std_logic;
		AO : out std_logic;
		BO : out std_logic;
		CO : out std_logic;
		DO : out std_logic;
		EO : out std_logic;
		IO : out std_logic;
		FO : out std_logic;
		GO : out std_logic;
		HO : out std_logic;
		JO : out std_logic
		);
	end component;
 
 	component dec_8b10b
	port(
		RESET : in std_logic;
		RBYTECLK : in std_logic;
		AI : in std_logic;
		BI : in std_logic;
		CI : in std_logic;
		DI : in std_logic;
		EI : in std_logic;
		II : in std_logic;
		FI : in std_logic;
		GI : in std_logic;
		HI : in std_logic;
		JI : in std_logic;
		AO : out std_logic;
		BO : out std_logic;
		CO : out std_logic;
		DO : out std_logic;
		EO : out std_logic;
		FO : out std_logic;
		GO : out std_logic;
		HO : out std_logic;
		KO : out std_logic
		);
	end component;
    
    -- FRAME GENERATOR
    component ONU_FRAME_GEN 
    port (
        CLK_I             : in  std_logic;  
        RESET_I           : in  std_logic;  

        -- CALIBRATION ENABLE SIGNALS
        CAL_ENA_I         : in  std_logic;   
        FAST_BEAT_I       : in  std_logic;
        SLOW_BEAT_I       : in  std_logic;
        
        -- STANDARD FRAME TX ENABLE SIGNAL
        FRAME_ENA_I       : in  std_logic;
        
        -- INPUT DATA WITH ONU ADDRESS
        TXDATA_I          : in  std_logic_vector(31 downto 0);
        ADDRESS_I         : in  std_logic_vector(7 downto 0);
        READY_O           : out std_logic;
        
        -- 8b10b ENCODED DATA
        TXDATA_O          : out std_logic_vector(31 downto 0);
        TX8B10BBYPASS_O   : out std_logic_vector(3 downto 0);
        TXCHARISK_O       : out std_logic_vector(3 downto 0)
        
        );
    end component;
         

    -- WORD ALIGNER
    component ONU_WORD_ALIGNER  
    generic (
            g_COMMAMASK                       : std_logic_vector(9 downto 0)  := "1111111111";   
            g_PCOMMAPATTERN                   : std_logic_vector(9 downto 0)  := "0101111100";     -- For K28.5 having Runing Disparity '-'
            g_NCOMMAPATTERN                   : std_logic_vector(9 downto 0)  := "1010000011";    -- For K28.5 having Runing Disparity '+'
            g_SEARCH_TO_LOCK_TOLERANCE_VALUE  : std_logic_vector(7 downto 0)  := x"24";
            g_LOCK_TO_SEARCH_TOLERANCE_VALUE  : std_logic_vector(7 downto 0)  := x"12"
            );
    port (
        -- CLOCKS
            LOGIC_CLK_I             : in  std_logic;
            RX_WORD_CLK_I           : in  std_logic;
            CLOCK_ENABLE_O          : out std_logic;
            
            RESET_I                 : in  std_logic;
            ENABLE_I                : in  std_logic;
            RX_DATA_I               : in  std_logic_vector(39 downto 0);

            RESET_PHY_O             : out std_logic;
            BITSLIP_O               : out std_logic;

        -- MONITOR SIGNALS
            ALIGNED_O               : out std_logic;        
            BITSLIP_NO_O            : out std_logic_vector(7 downto 0);
            WORD_ALIGNER_STATE_O    : out std_logic_vector(3 downto 0)
    );
    end component;
    


 
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
 
   --==================================== Port Mapping ==============================================--  
    encoder_decoder_8b10_genx4:
    for i in 0 to 3 generate
	encoder_8b10b_comp : 
    enc_8b10b 
		port map (
			RESET => TX_RESET,
			SBYTECLK => TX_PMA_WORD_CLK_I,
			KI => enc_8b10b_is_control_char_i(i),
			AI => enc_8b10b_data_i(i*8+0),
			BI => enc_8b10b_data_i(i*8+1),
			CI => enc_8b10b_data_i(i*8+2),
			DI => enc_8b10b_data_i(i*8+3),
			EI => enc_8b10b_data_i(i*8+4),
			FI => enc_8b10b_data_i(i*8+5),
			GI => enc_8b10b_data_i(i*8+6),
			HI => enc_8b10b_data_i(i*8+7),
			AO => enc_8b10b_data_o(i*10+0),
			BO => enc_8b10b_data_o(i*10+1),
			CO => enc_8b10b_data_o(i*10+2),
			DO => enc_8b10b_data_o(i*10+3),
			EO => enc_8b10b_data_o(i*10+4),
			IO => enc_8b10b_data_o(i*10+5),
			FO => enc_8b10b_data_o(i*10+6),
			GO => enc_8b10b_data_o(i*10+7),
			HO => enc_8b10b_data_o(i*10+8),
			JO => enc_8b10b_data_o(i*10+9)
		);

    decoder_8b10b_comp : 
    dec_8b10b 
		port map (
			RESET => RX_RESET,
			RBYTECLK => RX_PMA_WORD_CLK_I,
			AI => dec_8b10b_data_i(i*10+0),	-- Note: Use the latched encoded data
			BI => dec_8b10b_data_i(i*10+1),
			CI => dec_8b10b_data_i(i*10+2),
			DI => dec_8b10b_data_i(i*10+3),
			EI => dec_8b10b_data_i(i*10+4),
			II => dec_8b10b_data_i(i*10+5),
			FI => dec_8b10b_data_i(i*10+6),
			GI => dec_8b10b_data_i(i*10+7),
			HI => dec_8b10b_data_i(i*10+8),
			JI => dec_8b10b_data_i(i*10+9),
			AO => dec_8b10b_data_o(i*8+0),
			BO => dec_8b10b_data_o(i*8+1),
			CO => dec_8b10b_data_o(i*8+2),
			DO => dec_8b10b_data_o(i*8+3),
			EO => dec_8b10b_data_o(i*8+4),
			FO => dec_8b10b_data_o(i*8+5),
			GO => dec_8b10b_data_o(i*8+6),
			HO => dec_8b10b_data_o(i*8+7),
			KO => dec_8b10b_is_control_char_o(i)
		);
    end generate;

    
    
    onu_frame_gen_comp:
    ONU_FRAME_GEN 
        port map (
            CLK_I             => TX_PMA_WORD_CLK_I,
            RESET_I           => TX_RESET, 

            -- CALIBRATION ENABLE SIGNALS
            CAL_ENA_I         => onu_frame_gen_cal_ena_i,   
            FAST_BEAT_I       => onu_frame_gen_fast_beat_i,
            SLOW_BEAT_I       => onu_frame_gen_slow_beat_i,
            
            -- STANDARD FRAME TX ENABLE SIGNAL
            FRAME_ENA_I       => onu_frame_gen_frame_ena_i,
            
            -- INPUT DATA WITH ONU ADDRESS
            TXDATA_I          => onu_frame_gen_tx_data_i,
            ADDRESS_I         => onu_frame_gen_address_i,
            READY_O           => onu_frame_gen_ready_o,
            
            -- 8b10b ENCODED DATA
            TXDATA_O          => onu_frame_gen_txdata_o,
            TX8B10BBYPASS_O   => onu_frame_gen_tx8b10bypass_o,
            TXCHARISK_O       => onu_frame_gen_txcharisk_o
        
        );

    
    onu_word_align_comp:
    ONU_WORD_ALIGNER 
    generic map (
            g_COMMAMASK                       => "1111111111",   
            g_PCOMMAPATTERN                   => "0101111100",     -- For K28.5 having Runing Disparity '-'
            g_NCOMMAPATTERN                   => "1010000011",    -- For K28.5 having Runing Disparity '+'
            g_SEARCH_TO_LOCK_TOLERANCE_VALUE  => x"24",
            g_LOCK_TO_SEARCH_TOLERANCE_VALUE  => x"12"
            )
    port map (
        -- CLOCKS
            LOGIC_CLK_I             => USER_CLK_I,
            RX_WORD_CLK_I           => RX_PMA_WORD_CLK_I,
            CLOCK_ENABLE_O          => clock_enable,
            RESET_I                 => RX_RESET,
            ENABLE_I                => onu_word_aligner_enable_i,
            RX_DATA_I               => onu_word_aligner_rxdata_i,
            RESET_PHY_O             => onu_word_aligner_reset_gtx_o,
            BITSLIP_O               => onu_word_aligner_bitslip_o,
        -- MONITOR SIGNALS           
            ALIGNED_O               => onu_word_aligner_aligned_o,        
            BITSLIP_NO_O            => onu_word_aligner_bitslip_num_o,
            WORD_ALIGNER_STATE_O    => onu_word_aligner_state_o
            );
    

 
   --==============================================================================================--
   --=================================== RESET CONNECTIONS ========================================--
   --==============================================================================================--
    TX_RESET                    <= GENERAL_RESET_I or TX_PCS_RESET_I;
    RX_RESET                    <= GENERAL_RESET_I or RX_PCS_RESET_I;

   --==============================================================================================--
   --===================== DATA AND CONTROL SIGNAL CONNECTIONS ====================================--
   --==============================================================================================--
   -- TRANSMISSION SIDE
   -- USER DATA PAYLOAD----> ONU FRAME GENERATOR ---> 8b10b ENCODER --->   XCVR PHY ---> ...........
   
   -- ONU FRAME GENERATOR
    -- onu_frame_gen_cal_ena_i                          <= TTC_PCS_I.cal_ena_i;
    -- onu_frame_gen_fast_beat_i                        <= TTC_PCS_I.fast_beat_i;
    -- onu_frame_gen_slow_beat_i                        <= TTC_PCS_I.slow_beat_i;
    -- onu_frame_gen_frame_ena_i                        <= TTC_PCS_I.frame_ena_i;
    -- onu_frame_gen_tx_data_i                          <= TTC_PCS_I.tx_data_payload_i;
    -- onu_frame_gen_address_i                          <= TTC_PCS_I.onu_address_i;
    -- TTC_PCS_O.tx_ready_o                             <= onu_frame_gen_ready_o;  
        
    --8b10 Encoder     
    -- enc_8b10b_data_i                                 <= onu_frame_gen_txdata_o;
    -- enc_8b10b_is_control_char_i                      <= onu_frame_gen_txcharisk_o;
    
    proc_tx_frame_counter_gen:
    process(RX_RESET,RX_PMA_WORD_CLK_I)
    begin
        if RX_RESET = '1' then
            tx_frame_to_word_gen_counter     <= x"0";
        elsif rising_edge(RX_PMA_WORD_CLK_I) then
            if tx_frame_to_word_gen_counter = x"5" then
                tx_frame_to_word_gen_counter <= x"0";
            else 
                tx_frame_to_word_gen_counter     <= tx_frame_to_word_gen_counter + '1';
            end if;
        end if;
    end process;
    
    
    
    proc_gen_tx_word:
    process(TX_PMA_WORD_CLK_I)
    begin
    
        if rising_edge(TX_PMA_WORD_CLK_I) then
            if tx_frame_to_word_gen_counter = x"0" then
                enc_8b10b_data_i                                 <= TTC_PCS_I.tx_frame_i(191 downto 160);
                enc_8b10b_is_control_char_i                      <= "1000";
            elsif tx_frame_to_word_gen_counter = x"1" then
                enc_8b10b_data_i                                 <= TTC_PCS_I.tx_frame_i(159 downto 128);
                enc_8b10b_is_control_char_i                      <= "0000";
            elsif tx_frame_to_word_gen_counter = x"2" then
                enc_8b10b_data_i                                 <= TTC_PCS_I.tx_frame_i(127 downto 96);
                enc_8b10b_is_control_char_i                      <= "0000";    
            elsif tx_frame_to_word_gen_counter = x"3" then
                enc_8b10b_data_i                                 <= TTC_PCS_I.tx_frame_i(95 downto 64);
                enc_8b10b_is_control_char_i                      <= "0000";     
            elsif tx_frame_to_word_gen_counter = x"4" then
                enc_8b10b_data_i                                 <= TTC_PCS_I.tx_frame_i(63 downto 32);
                enc_8b10b_is_control_char_i                      <= "0000";    
            else
                enc_8b10b_data_i                                 <= TTC_PCS_I.tx_frame_i(31 downto 0);
                enc_8b10b_is_control_char_i                      <= "0000";                
            end if;
        end if;
    end process;

    
    
    -- process(TX_PMA_WORD_CLK_I)
    -- begin
        -- if rising_edge(TX_PMA_WORD_CLK_I) then
            -- if onu_frame_gen_tx8b10bypass_o = "1111" then
                -- TTC_PCS_O.tx_parallel_data_o        <= (others=>'0');
            -- else
                -- TTC_PCS_O.tx_parallel_data_o        <= enc_8b10b_data_o;
            -- end if;
        -- end if;        
    -- end process;
    
    TTC_PCS_O.tx_parallel_data_o        <= enc_8b10b_data_o;

   --========================================================================================================--
   -- RECEIVER SIDE
   -- ........... ---> XCVR PHY ---> WORD ALIGNER ---> 8b10b DECODER ---> USER DATA PAYLOAD

          
            
    -- WORD ALIGNER
    onu_word_aligner_enable_i                        <= TTC_PCS_I.rx_phy_ready_i and TTC_PCS_I.onu_word_aligner_enable_i;
    onu_word_aligner_rxdata_i                        <= TTC_PCS_I.rx_parallel_data_i;
    TTC_PCS_O.aligned_o                              <= onu_word_aligner_aligned_o;
    TTC_PCS_O.reset_gtx_o                            <= onu_word_aligner_reset_gtx_o;
    TTC_PCS_O.bitslip_o                              <= onu_word_aligner_bitslip_o;
    TTC_PCS_O.bitslip_num_o                          <= onu_word_aligner_bitslip_num_o;
    TTC_PCS_O.word_aligner_state_o                   <= onu_word_aligner_state_o;
    
    -- 8b10b DECODER
    dec_8b10b_data_i                                 <= TTC_PCS_I.rx_parallel_data_i;
        
    proc_align_word_and_clock_enable:
    process(RX_PMA_WORD_CLK_I)    
    begin
        if rising_edge(RX_PMA_WORD_CLK_I) then
            TTC_PCS_O.rx_data_payload_o                      <= dec_8b10b_data_o;
            TTC_PCS_O.is_control_char_o                      <= dec_8b10b_is_control_char_o;
            CLOCK_ENABLE_O                                   <= clock_enable;
        end if;
    end process;

    proc_rx_frame_counter_gen:
    process(RX_RESET,RX_PMA_WORD_CLK_I)
    begin
        if RX_RESET = '1' then
            rx_word_to_frame_gen_counter     <= x"0";
        elsif rising_edge(RX_PMA_WORD_CLK_I) then
            if clock_enable = '1' or rx_word_to_frame_gen_counter = x"5" then
                rx_word_to_frame_gen_counter <= x"0";
            else 
                rx_word_to_frame_gen_counter     <= rx_word_to_frame_gen_counter + '1';
            end if;
        end if;
    end process;
    
    
    dec_8b10b_data_o_byte_flip                   <= dec_8b10b_data_o(7 downto 0) & dec_8b10b_data_o(15 downto 8) & dec_8b10b_data_o(23 downto 16) & dec_8b10b_data_o(31 downto 24) ;
    
    proc_gen_rx_frame_word:
    process(RX_PMA_WORD_CLK_I)
    begin
    
        if rising_edge(RX_PMA_WORD_CLK_I) then
            if rx_word_to_frame_gen_counter = x"4" then
                rx_frame_o_buf(191 downto 160)                            <= dec_8b10b_data_o_byte_flip;
            elsif rx_word_to_frame_gen_counter = x"5" then
                rx_frame_o_buf(159 downto 128)                            <= dec_8b10b_data_o_byte_flip;
            elsif rx_word_to_frame_gen_counter = x"0" then
                rx_frame_o_buf(127 downto 96)                             <= dec_8b10b_data_o_byte_flip;
            elsif rx_word_to_frame_gen_counter = x"1" then
                rx_frame_o_buf(95 downto 64)                              <= dec_8b10b_data_o_byte_flip;
            elsif rx_word_to_frame_gen_counter = x"2" then
                rx_frame_o_buf(63 downto 32)                              <= dec_8b10b_data_o_byte_flip;
            else
                rx_frame_o_buf(31 downto 0)                               <= dec_8b10b_data_o_byte_flip;                
            end if;
        end if;
    end process;
    
    proc_gen_rx_frame:
    process(RX_PMA_WORD_CLK_I, clock_enable)
    begin
        if rising_edge(RX_PMA_WORD_CLK_I) and clock_enable = '1' then
            TTC_PCS_O.rx_frame_o            <= rx_frame_o_buf;
        end if;
    end process;
    
end architecture mixed;