library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--==============================================================================
--! Entity declaration for PRBS checker
--==============================================================================
entity prbs_data_check  is
    generic (
        G_DIN_WIDTH    : integer := 20;
		G_REVERSE_BITS : integer := 0);
    port (
		clock_i           : in  std_logic;
		reset_i           : in  std_logic;
        ena_i             : in  std_logic;
        sel_i             : in  std_logic_vector(1 downto 0);
        -- "00" PRBS-7, "01" PRBS-23
        -- "1x" fix
		din_i             : in  std_logic_vector(G_DIN_WIDTH-1 downto 0);
		lock_o            : out std_logic;
		error_o           : out std_logic;
		nerrs_o           : out std_logic_vector(7 downto 0);
		refgen_o          : out std_logic_vector(G_DIN_WIDTH-1 downto 0));

end entity prbs_data_check ;

--==============================================================================
-- architecture declaration
--==============================================================================
architecture RTL of prbs_data_check is

	signal ZEROS : std_logic_vector(127 downto 0);

    function slv_bitreverse (slv : in std_logic_vector) return std_logic_vector is
        variable slv_ret : std_logic_vector(slv'RANGE);
        alias slv_rr     : std_logic_vector(slv'REVERSE_RANGE) is slv;
    begin
        for i in slv'RANGE loop
            slv_ret(i) := slv_rr(i);
        end loop;
        return slv_ret;
    end function slv_bitreverse;

    function bitcount ( signal slv : std_logic_vector(3 downto 0)) return integer is
    begin  -- bitcount
        case slv is
            when "0000" => return 0;
            when "0001" => return 1;
            when "0010" => return 1;
            when "0011" => return 2;
            when "0100" => return 1;
            when "0101" => return 2;
            when "0110" => return 2;
            when "0111" => return 3;
            when "1000" => return 1;
            when "1001" => return 2;
            when "1010" => return 2;
            when "1011" => return 3;
            when "1100" => return 2;
            when "1101" => return 3;
            when "1110" => return 3;
            when "1111" => return 4;
            when others => return 0;
        end case;
    end bitcount;
    
    component prbs_data_gen
        generic (
            G_DOUT_WIDTH   : integer := 20;
            G_PRBS_TAP     : integer := 0;
            G_REVERSE_BITS : integer := 0);
        port (
            clock_i : in std_logic;
            mode_i  : in std_logic;  -- '0' external seed, '1' internal loop-back
            ena_i   : in std_logic;  -- '0' hold, '1' generate
            sel_i   : in std_logic_vector(1 downto 0);
            seed_i  : in std_logic_vector(22 downto 0);
            dout_o  : out std_logic_vector(G_DOUT_WIDTH-1 downto 0));
    
    end component prbs_data_gen ;
	
	signal refgen_mode   : std_logic;
    signal refgen_ena    : std_logic;
    signal refgen_sel    : std_logic_vector(1 downto 0);
	signal refgen_seed   : std_logic_vector(22 downto 0);
    signal refgen_dout   : std_logic_vector(G_DIN_WIDTH-1 downto 0);
    signal datain        : std_logic_vector(G_DIN_WIDTH-1 downto 0);
    signal datain_r      : std_logic_vector(G_DIN_WIDTH-1 downto 0);
    signal ena_r         : std_logic;
    signal ena_rr        : std_logic;
    signal data_error    : std_logic;
    signal search_mode   : std_logic;
    signal errd_bits     : std_logic_vector(95 downto 0);
--==============================================================================
-- architecture begin
--==============================================================================
begin  -- architecture RTL

	ZEROS <= (others => '0');

    no_reverse : if G_REVERSE_BITS = 0 generate
        datain <= din_i;
    end generate no_reverse;

    reverse : if G_REVERSE_BITS = 1 generate
        datain <= slv_bitreverse(din_i);
    end generate reverse;

	p_datain_reg: process(clock_i, reset_i) is
	begin
		if reset_i = '1' then
			datain_r <= (others => '1');
			ena_r <= '0';
			ena_rr <= '0';
		elsif clock_i'event and clock_i = '1' then
    	    datain_r <= datain;
    	    ena_r <= ena_i;
    	    ena_rr <= ena_r;
		end if;
	end process p_datain_reg;
	
    DIN_WIDER: if G_DIN_WIDTH >= 23 generate
        refgen_seed <= datain(refgen_seed'range);
    end generate DIN_WIDER;
    SEED_WIDER: if G_DIN_WIDTH < 23 generate
        refgen_seed <= datain_r((23-G_DIN_WIDTH)-1 downto 0) & datain;
    end generate SEED_WIDER;
	refgen_mode <= not search_mode;
    refgen_ena  <= ena_i;
	refgen_sel  <= sel_i;
	cmp_prbs_gen: prbs_data_gen
		generic map (
			G_DOUT_WIDTH => G_DIN_WIDTH,
			G_PRBS_TAP   => 0)
		port map (
			clock_i => clock_i,
			mode_i  => refgen_mode,
            ena_i   => refgen_ena,
			sel_i   => refgen_sel,
			seed_i  => refgen_seed,
			dout_o  => refgen_dout);

    p_compare: process (clock_i, reset_i) is
        variable good_data_count : integer range 0 to 15;
        variable bad_data_count  : integer range 0 to 15;
    begin  -- process p_regs
        if reset_i = '1' then         -- asynchronous reset (active high)
            search_mode <= '1';
            data_error <= '1';
            good_data_count := 0;
            bad_data_count  := 0;
        elsif clock_i'event and clock_i = '1' then  -- rising clock edge
            if ena_r = '0' then
                data_error <= '0';
            elsif datain_r /= refgen_dout then
                data_error <= '1';
            else
                data_error <= '0';
            end if;
            if search_mode = '1' and data_error = '0' and ena_rr = '1' then
                -- in searching mode and good data
                if good_data_count < 8 then
                    good_data_count := good_data_count + 1;
                else
                    search_mode <= '0';
                    good_data_count := 0;
                    bad_data_count  := 0;
                end if;
            elsif search_mode = '1' and data_error = '1' and ena_rr = '1' then
                -- in searching mode and bad data
                good_data_count := 0;
            elsif search_mode = '0' and data_error = '1' and ena_rr = '1' then
                -- in locked state and bad data
                if bad_data_count < 8 then
                    bad_data_count := bad_data_count + 1;
                else
                    search_mode <= '1';
                    bad_data_count := 0;
                    good_data_count := 0;
                end if;
            elsif search_mode = '0' and data_error = '0' and ena_rr = '1' then
                -- in locked state and good data
                bad_data_count := 0;
            end if;
        end if;
    end process p_compare;
	lock_o <= not search_mode;
	error_o <= data_error;

    p_error_count: process (clock_i, reset_i)
        variable bc_03_00 : integer range 0 to 7 := 0;  -- max value is 4
        variable bc_07_04 : integer range 0 to 7 := 0;
        variable bc_11_08 : integer range 0 to 7 := 0;
        variable bc_15_12 : integer range 0 to 7 := 0;
        variable bc_19_16 : integer range 0 to 7 := 0;
        variable bc_23_20 : integer range 0 to 7 := 0;
        variable bc_27_24 : integer range 0 to 7 := 0;
        variable bc_31_28 : integer range 0 to 7 := 0;
        variable bc_35_32 : integer range 0 to 7 := 0;
        variable bc_39_36 : integer range 0 to 7 := 0;
        variable bc_43_40 : integer range 0 to 7 := 0;
        variable bc_47_44 : integer range 0 to 7 := 0;
        variable bc_51_48 : integer range 0 to 7 := 0;
        variable bc_55_52 : integer range 0 to 7 := 0;
        variable bc_59_56 : integer range 0 to 7 := 0;
        variable bc_63_60 : integer range 0 to 7 := 0;
        variable bc_67_64 : integer range 0 to 7 := 0;
        variable bc_71_68 : integer range 0 to 7 := 0;
        variable bc_75_72 : integer range 0 to 7 := 0;
        variable bc_79_76 : integer range 0 to 7 := 0;
        variable bc_83_80 : integer range 0 to 7 := 0;
        variable bc_87_84 : integer range 0 to 7 := 0;
        variable bc_91_88 : integer range 0 to 7 := 0;
        variable bc_95_92 : integer range 0 to 7 := 0;
        variable bc_15_00 : integer range 0 to 31 := 0;  -- max value is 4*4=16
        variable bc_31_16 : integer range 0 to 31 := 0;
        variable bc_47_32 : integer range 0 to 31 := 0;
        variable bc_63_48 : integer range 0 to 31 := 0;
        variable bc_79_64 : integer range 0 to 31 := 0;
        variable bc_95_80 : integer range 0 to 31 := 0;
        variable bc_31_00 : integer range 0 to 63 := 0;  -- max value is 2*16=32
        variable bc_63_32 : integer range 0 to 63 := 0;
        variable bc_95_64 : integer range 0 to 63 := 0;
        variable bc_95_00 : integer range 0 to 127 := 0;  -- max value is 84
    begin  -- process p_error_count
        if reset_i = '1' then  -- asynchronous reset (active high)
            nerrs_o   <= (others => '0');
            errd_bits <= (others => '0');
            bc_95_00 := 0;
            bc_31_00 := 0;
            bc_63_32 := 0;
            bc_95_64 := 0;
            bc_15_00 := 0;
            bc_31_16 := 0;
            bc_47_32 := 0;
            bc_63_48 := 0;
            bc_79_64 := 0;
            bc_95_80 := 0;
            bc_03_00 := 0;
            bc_07_04 := 0;
            bc_11_08 := 0;
            bc_15_12 := 0;
            bc_19_16 := 0;
            bc_23_20 := 0;
            bc_27_24 := 0;
            bc_31_28 := 0;
            bc_35_32 := 0;
            bc_39_36 := 0;
            bc_43_40 := 0;
            bc_47_44 := 0;
            bc_51_48 := 0;
            bc_55_52 := 0;
            bc_59_56 := 0;
            bc_63_60 := 0;
            bc_67_64 := 0;
            bc_71_68 := 0;
            bc_75_72 := 0;
            bc_79_76 := 0;
            bc_83_80 := 0;
            bc_87_84 := 0;
            bc_91_88 := 0;
            bc_95_92 := 0;
        elsif clock_i'event and clock_i = '1' then  -- rising clock edge
            errd_bits <= (others => '0');
            if ena_r = '1' then
                errd_bits(datain_r'range) <= datain_r xor refgen_dout;
            end if;

            -- cycle 4
            bc_95_00 := bc_31_00 + bc_63_32 + bc_95_64;
            nerrs_o <= std_logic_vector(to_unsigned(bc_95_00, nerrs_o'length));

            -- cycle 3
            bc_31_00 := bc_15_00 + bc_31_16;
            bc_63_32 := bc_47_32 + bc_63_48;
            bc_95_64 := bc_79_64 + bc_95_80;

            -- cycle 2
            bc_15_00 := bc_03_00 + bc_07_04 + bc_11_08 + bc_15_12;
            bc_31_16 := bc_19_16 + bc_23_20 + bc_27_24 + bc_31_28;
            bc_47_32 := bc_35_32 + bc_39_36 + bc_43_40 + bc_47_44;
            bc_63_48 := bc_51_48 + bc_55_52 + bc_59_56 + bc_63_60;
            bc_79_64 := bc_67_64 + bc_71_68 + bc_75_72 + bc_79_76;
            bc_95_80 := bc_83_80 + bc_87_84 + bc_91_88 + bc_95_92;

            -- cycle 1
            bc_03_00 := bitcount(errd_bits(3 downto 0));
            bc_07_04 := bitcount(errd_bits(7 downto 4));
            bc_11_08 := bitcount(errd_bits(11 downto 8));
            bc_15_12 := bitcount(errd_bits(15 downto 12));
            bc_19_16 := bitcount(errd_bits(19 downto 16));
            bc_23_20 := bitcount(errd_bits(23 downto 20));
            bc_27_24 := bitcount(errd_bits(27 downto 24));
            bc_31_28 := bitcount(errd_bits(31 downto 28));
            bc_35_32 := bitcount(errd_bits(35 downto 32));
            bc_39_36 := bitcount(errd_bits(39 downto 36));
            bc_43_40 := bitcount(errd_bits(43 downto 40));
            bc_47_44 := bitcount(errd_bits(47 downto 44));
            bc_51_48 := bitcount(errd_bits(51 downto 48));
            bc_55_52 := bitcount(errd_bits(55 downto 52));
            bc_59_56 := bitcount(errd_bits(59 downto 56));
            bc_63_60 := bitcount(errd_bits(63 downto 60));
            bc_67_64 := bitcount(errd_bits(67 downto 64));
            bc_71_68 := bitcount(errd_bits(71 downto 68));
            bc_75_72 := bitcount(errd_bits(75 downto 72));
            bc_79_76 := bitcount(errd_bits(79 downto 76));
            bc_83_80 := bitcount(errd_bits(83 downto 80));
            bc_87_84 := bitcount(errd_bits(87 downto 84));
            bc_91_88 := bitcount(errd_bits(91 downto 88));
            bc_95_92 := bitcount(errd_bits(95 downto 92));
        end if;
    end process p_error_count;

    refgen_o <= refgen_dout;	
end architecture RTL;
--==============================================================================
-- architecture end
--==============================================================================
