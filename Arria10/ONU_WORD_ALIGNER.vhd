--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
-------------------------------------------------------------------------------
-- Title      : Rx Word Aligner
-- Project    : TTC-PON 2015
-------------------------------------------------------------------------------
-- File       : ONU_WORD_ALIGNER.vhd
-- Author     : Jubin MITRA (jmitra@cern.ch)
-- Company    : VECC
-- Created    : 14-04-2016
-- Last update: 14-04-2016
-- Platform   : 
-- Standard   : VHDL'93/08                  
-- Revision   : 1.0                                                     
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.1                                                                
-------------------------------------------------------------------------------
-- Description: 
-- It aligns the word by detecting 8b10b k28.5 comma character and 
-- state machine does bitslip and reset of transceiver in such a way that
-- deterministic latency is maintained						
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 14-04-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.TTC_PON_PACKAGE.all;
--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity ONU_WORD_ALIGNER is 
generic (
        g_COMMAMASK                       : std_logic_vector(9 downto 0)  := "0001111111";  -- comma mask to detect for both K28.5 and K28.1 
        g_PCOMMAPATTERN                   : std_logic_vector(9 downto 0)  := "0101111100";     -- For K28.5 having Runing Disparity '-'
        g_NCOMMAPATTERN                   : std_logic_vector(9 downto 0)  := "1010000011";    -- For K28.5 having Runing Disparity '+'
        g_SEARCH_TO_LOCK_TOLERANCE_VALUE  : std_logic_vector(7 downto 0)  := x"24";
        g_LOCK_TO_SEARCH_TOLERANCE_VALUE  : std_logic_vector(7 downto 0)  := x"12"
        );
port (
    -- CLOCKS
        LOGIC_CLK_I             : in  std_logic;
        RX_WORD_CLK_I           : in  std_logic;
        CLOCK_ENABLE_O          : out std_logic;
        
        RESET_I                 : in  std_logic;
        ENABLE_I                : in  std_logic;
        RX_DATA_I               : in  std_logic_vector(39 downto 0);

        RESET_PHY_O             : out std_logic;
        BITSLIP_O               : out std_logic;

    -- MONITOR SIGNALS
        ALIGNED_O               : out std_logic;        
        BITSLIP_NO_O            : out std_logic_vector(7 downto 0);
        WORD_ALIGNER_STATE_O    : out std_logic_vector(3 downto 0)
);
end entity ONU_WORD_ALIGNER;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture mixed of ONU_WORD_ALIGNER is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
    type t_alignment_state is (
    IDLE,
    SEARCH,
    ASSERT_GTX_BITSLIP,
    DEASSERT_GTX_BITSLIP,
    WAIT_AFTER_BITSLIP,
    IS_ALIGNED,
    RESET_GTX,
    WAIT_AFTER_RESET,
    LOCK);
    
    signal s_word_aligner_FSM_state    : t_alignment_state;
    signal s_algined                   : std_logic;
    signal s_bitslip_counter           : std_logic_vector(7 downto 0);

    signal s_lock_tolerance_counter    : std_logic_vector(7 downto 0);
    signal s_search_tolerance_counter  : std_logic_vector(7 downto 0);
    signal after_bitslip_wait_counter  : std_logic_vector(7 downto 0);
    signal after_reset_wait_counter    : std_logic_vector(7 downto 0);
    
    signal s_is_comma_detected          : std_logic;
    signal s_is_comma_detected_buf      : std_logic_vector(5 downto 0);
    signal s_is_frame_aligned           : std_logic;

    signal s_bitslip                   : std_logic;
    signal s_reset_gtx                 : std_logic;
    
    signal mod_6_word_position_counter : std_logic_vector(3 downto 0):="0000";
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
 
    proc_mod_6_counter:
    process(RESET_I,RX_WORD_CLK_I)
    begin
        if RESET_I = '1' then
            mod_6_word_position_counter <= x"0";
        elsif rising_edge(RX_WORD_CLK_I) then
            if s_is_comma_detected ='1' or mod_6_word_position_counter = x"5" then
                mod_6_word_position_counter <= x"0";
            else
                mod_6_word_position_counter <= mod_6_word_position_counter + '1';
            end if;
            
            if mod_6_word_position_counter = x"4" and s_algined = '1' then
                CLOCK_ENABLE_O              <= '1';
            else
                CLOCK_ENABLE_O              <= '0';
            end if;
            
        end if;
    end process;
    
    proc_is_comma_aligned:
    process(RX_WORD_CLK_I)
    begin
        if rising_edge(RX_WORD_CLK_I) then
            if (RX_DATA_I(9 downto 0) and g_COMMAMASK) = (g_PCOMMAPATTERN and g_COMMAMASK) or (RX_DATA_I(9 downto 0) and g_COMMAMASK) = (g_NCOMMAPATTERN and g_COMMAMASK) then
                s_is_comma_detected         <=  '1';
            else   
                s_is_comma_detected         <=  '0';
            end if;
            s_is_comma_detected_buf         <= s_is_comma_detected_buf(4 downto 0) & s_is_comma_detected;
            s_is_frame_aligned              <= s_is_comma_detected_buf(5) or s_is_comma_detected_buf(4)  or s_is_comma_detected_buf(3)  or s_is_comma_detected_buf(2)  or s_is_comma_detected_buf(1)  or s_is_comma_detected_buf(0);
        end if;
    end process;
 
    proc_word_aligner_FSM:
    process(RESET_I,LOGIC_CLK_I)
    begin
        if RESET_I = '1' then
            s_algined                    <=                       '0' ;
            s_bitslip_counter            <=            (others => '0');
            s_reset_gtx                  <=                       '0';
            s_bitslip                    <=                       '0';
            s_word_aligner_FSM_state     <=                       IDLE;
            s_lock_tolerance_counter     <=                     x"00";
            s_search_tolerance_counter   <=                     x"00";
                
        elsif rising_edge(LOGIC_CLK_I) then
            if ENABLE_I='1' then 
                case(s_word_aligner_FSM_state) is
                    when IDLE =>
                        if s_is_frame_aligned = '1' and s_bitslip_counter /= x"00" then     -- As initially if it gets aligned word, it must do bit slip to check for it                  
                        
                            s_lock_tolerance_counter     <= x"00";
                            s_search_tolerance_counter   <= x"00";                       
                        else 
                        
                            s_lock_tolerance_counter        <= s_lock_tolerance_counter + '1';
                            
                            if s_lock_tolerance_counter = g_LOCK_TO_SEARCH_TOLERANCE_VALUE  then
                                s_word_aligner_FSM_state    <= SEARCH;
                            end if;
                        end if;
                        
                    when SEARCH =>
                        s_algined                           <= '0';
                        s_bitslip_counter                   <= (others => '0'); 
                        s_bitslip                           <= '0';
                        s_reset_gtx                         <= '0';
                        s_lock_tolerance_counter            <= x"00";
                        s_search_tolerance_counter          <= x"00";
                        s_word_aligner_FSM_state            <= ASSERT_GTX_BITSLIP;
                        
                    when ASSERT_GTX_BITSLIP =>
                        s_bitslip                          <= '1';
                        s_bitslip_counter                  <= s_bitslip_counter + '1';
                        s_word_aligner_FSM_state           <= DEASSERT_GTX_BITSLIP;
                         
                    when DEASSERT_GTX_BITSLIP =>
                        s_bitslip                          <= '0';
                        s_word_aligner_FSM_state           <= WAIT_AFTER_BITSLIP;
                        after_bitslip_wait_counter         <= x"00";

                    when WAIT_AFTER_BITSLIP =>
                        after_bitslip_wait_counter         <= after_bitslip_wait_counter + '1';
                        if after_bitslip_wait_counter = x"25" then
                            s_word_aligner_FSM_state           <= IS_ALIGNED;
                        end if;
                        
                    when IS_ALIGNED => 
                        if s_is_frame_aligned = '1' then                       
                            if s_bitslip_counter(0) = '1' then
                                s_search_tolerance_counter         <= x"00"; 
                                s_word_aligner_FSM_state           <= LOCK;    
                            else 
                                s_word_aligner_FSM_state           <= RESET_GTX;  
                            end if;
                        else 
                                s_word_aligner_FSM_state           <= ASSERT_GTX_BITSLIP;                                                        
                        end if;
                    
                    when RESET_GTX =>
                        s_reset_gtx                                 <= '1';
                        after_reset_wait_counter                    <= x"00";
                        s_word_aligner_FSM_state                    <= WAIT_AFTER_RESET;

                    when WAIT_AFTER_RESET =>
                        s_reset_gtx                                 <= '0';
                        after_reset_wait_counter                    <= after_reset_wait_counter + '1';
                        
                        if after_reset_wait_counter = x"FF" then
                            s_word_aligner_FSM_state           <= SEARCH;
                        end if;
                        
                    when LOCK =>
                        if s_is_frame_aligned = '1' then                                
                            s_search_tolerance_counter         <= s_search_tolerance_counter + '1';
                        else
                            s_word_aligner_FSM_state           <= SEARCH;                     
                        end if;
                        
                        if s_search_tolerance_counter = g_SEARCH_TO_LOCK_TOLERANCE_VALUE then
                            s_word_aligner_FSM_state            <= IDLE; 
                            s_algined                           <= '1';
                        end if;
                        
                    when others =>
                        s_word_aligner_FSM_state    <= IDLE;
                end case;
            else
                        s_algined                           <= '0';
                        s_bitslip                           <= '0';
                        s_reset_gtx                         <= '0';
            end if;
        end if;
        
    end process;
    
    BITSLIP_O                    <= s_bitslip; 
    RESET_PHY_O                  <= s_reset_gtx;
    ALIGNED_O                    <= s_algined;
    BITSLIP_NO_O                 <= s_bitslip_counter;
    WORD_ALIGNER_STATE_O         <=   "0000" when s_word_aligner_FSM_state=IDLE
                                 else "0001" when s_word_aligner_FSM_state=SEARCH
                                 else "0010" when s_word_aligner_FSM_state=ASSERT_GTX_BITSLIP
                                 else "0011" when s_word_aligner_FSM_state=DEASSERT_GTX_BITSLIP
                                 else "0100" when s_word_aligner_FSM_state=WAIT_AFTER_BITSLIP
                                 else "0101" when s_word_aligner_FSM_state=IS_ALIGNED
                                 else "0110" when s_word_aligner_FSM_state=RESET_GTX
                                 else "0111" when s_word_aligner_FSM_state=WAIT_AFTER_RESET
                                 else "1000" when s_word_aligner_FSM_state=LOCK;

end architecture mixed;